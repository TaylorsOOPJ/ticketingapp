package my.edu.taylors.oopj.movieticketing.service;

import my.edu.taylors.oopj.movieticketing.entity.Payment;
import my.edu.taylors.oopj.movieticketing.entity.ShowTime;

/**
 *
 * @author Johnny Chan
 */
public interface TicketBookingService {

	ShowTime getShowTime(int showTimeId);

	void savePayment(Payment payment);

}
