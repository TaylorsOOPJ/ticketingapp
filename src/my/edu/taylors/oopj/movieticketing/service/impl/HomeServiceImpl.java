package my.edu.taylors.oopj.movieticketing.service.impl;

import java.util.List;

import my.edu.taylors.oopj.movieticketing.dao.TheaterDao;
import my.edu.taylors.oopj.movieticketing.dao.impl.TheaterDaoImpl;
import my.edu.taylors.oopj.movieticketing.entity.Theater;
import my.edu.taylors.oopj.movieticketing.service.HomeService;

public class HomeServiceImpl implements HomeService {

	private TheaterDao theaterDao;

	public HomeServiceImpl() {
		this.theaterDao = new TheaterDaoImpl();
	}

	@Override
	public List<Theater> getAllTheaters() {
		return this.theaterDao.findAll();
	}

}
