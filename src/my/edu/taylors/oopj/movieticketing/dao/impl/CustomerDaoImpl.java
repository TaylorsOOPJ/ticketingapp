package my.edu.taylors.oopj.movieticketing.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import my.edu.taylors.oopj.movieticketing.dao.CustomerDao;
import my.edu.taylors.oopj.movieticketing.db.DatabaseConnection;
import my.edu.taylors.oopj.movieticketing.entity.Customer;

public class CustomerDaoImpl extends AbstractDao<Customer> implements CustomerDao {

	public static final String SQL = "c.id, c.firstName, c.lastName, c.email, c.password, c.phone, c.registrationDate";

	public static Customer getCustomerFromResult(ResultSet results) throws SQLException {
		int id = results.getInt("c.id");
		String firstName = results.getString("c.firstName");
		String lastName = results.getString("c.lastName");
		String email = results.getString("c.email");
		String password = results.getString("c.password");
		int phone = results.getInt("c.phone");

		Timestamp registrationDateStamp = results.getTimestamp("c.registrationDate");
		Date registrationDate = new Date(registrationDateStamp.getTime());

		return new Customer(id, firstName, lastName, email, password, phone, registrationDate);
	}

	@Override
	public Customer getByKey(int key) {
		List<Customer> customers = find("WHERE c.id = ?", key);
		return customers.size() > 0 ? customers.get(0) : null;
	}

	@Override
	public List<Customer> findAll() {
		return find("");
	}

	@Override
	public Customer getByEmail(String email) {
		List<Customer> customers = find("WHERE c.email = ?", email);
		return customers.size() > 0 ? customers.get(0) : null;
	}

	private List<Customer> find(String where, Object... args) {

		try {
			Connection conn = DatabaseConnection.getInstance().getConnection();
			String sql = 
				"SELECT " + SQL + " " +
				"FROM customer c " + where;
			PreparedStatement selectStatement = conn.prepareStatement(sql);
			for (int i = 0; i < args.length; i++) {
				selectStatement.setObject(i+1, args[i]);
			}

			ResultSet results = selectStatement.executeQuery();
			List<Customer> customers = new ArrayList<>();

			while (results.next()) {
				Customer customer = getCustomerFromResult(results);
				customers.add(customer);
			}

			return customers;
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return new ArrayList<>(0);
	}

}
