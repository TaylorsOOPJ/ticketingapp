package my.edu.taylors.oopj.movieticketing;

import javax.swing.JFrame;

import javafx.application.Application;
import javafx.stage.Stage;
import my.edu.taylors.oopj.movieticketing.controller.LoginController;
import my.edu.taylors.oopj.movieticketing.db.DatabaseConnection;

public class Launcher extends Application {

	private static JFrame loadingWindow;

	/**
	 * For JavaFX - requires empty constructor
	 */
	public Launcher() { }

	/**
	 * Launcher for application
	 * @param args
	 */
	public Launcher(String[] args) {
		launch(args);
	}

	/**
	 * On start navigate to default controller and stage
	 */
	@Override
	public void start(Stage primaryStage) {
		Router.getInstance().navigate(new LoginController());

		loadingWindow.setVisible(false);
		loadingWindow.dispose();// must be; otherwise app is not ending
		loadingWindow = null;
	}


	/**
	 * Start method, connect to database a launch the application
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) {
		loadingWindow = new LoadingWindow();

		DatabaseConnection.getInstance().connect();

		new Launcher(args);
	}

}
