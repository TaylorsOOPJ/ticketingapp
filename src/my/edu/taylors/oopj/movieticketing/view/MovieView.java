package my.edu.taylors.oopj.movieticketing.view;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.List;

import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebView;
import my.edu.taylors.oopj.movieticketing.controller.ProfileController;
import my.edu.taylors.oopj.movieticketing.controller.TicketBookingController;
import my.edu.taylors.oopj.movieticketing.entity.Genre;
import my.edu.taylors.oopj.movieticketing.entity.Movie;
import my.edu.taylors.oopj.movieticketing.entity.ShowTime;
import my.edu.taylors.oopj.movieticketing.utils.AlertFactory;
import my.edu.taylors.oopj.movieticketing.view.listener.MovieViewListener;

public class MovieView extends View {

    public MovieView(MovieViewListener movieListener, int movieId) {

        Movie movie = movieListener.getMovie(movieId);
        List<Genre> genres = movieListener.getMovieGenres(movieId);

        List<ShowTime> showTimes = movieListener.getShowTimesByMovieAndTheater(movieId, HomeView.currentTheater);
        Node showTimeBox = getShowTimes(showTimes);

        String genresString = "";
        for (int i = 0; i < genres.size(); i++) {
            if (i > 0) genresString += ", ";
            genresString += genres.get(i).getName();
        }

        ScrollPane root = new ScrollPane();
        GridPane pane = new GridPane();
        VBox leftBox = new VBox();
        VBox movieBox = new VBox();
        VBox descBox = new VBox();
        StackPane titlebox = new StackPane();

        //Components
        //title
        Label mtitle = new Label(movie.getMovieName());
        mtitle.setId("title");

        //description
        Label mdesc = new Label(movie.getDescription());
        mdesc.setWrapText(true);
        mdesc.setId("p");
        mdesc.setPrefWidth(550);
        
        //imdb link
        Hyperlink imdb = new Hyperlink("IMDB page");
        imdb.setId("h3");
        imdb.setPadding(new Insets(0, 0, 15, 0));
        imdb.setOnAction(e -> {
            try {
                Desktop.getDesktop().browse(new URI(movie.getImdb()));
            } catch (IOException | URISyntaxException e1) {
                e1.printStackTrace();
                AlertFactory.createAlert(Alert.AlertType.ERROR, "Error", "Link cannot be open.");
            }
        });

        //trailer
        WebView wv = new WebView();
        wv.getEngine().loadContent("<iframe width=\"600\" height=\"337.5\" src=\""+movie.getTrailer()+"\" frameborder=\"0\"></iframe>");

        //poster
        ImageView poster = new ImageView(new Image(movie.getImg()));
        poster.setFitHeight(444);
        poster.setFitWidth(300);

        //rating
        Label rating = new Label("RATING");
        rating.setId("h4");
        Label rating2 = new Label(movie.getMovieRating() + " / 10");
        rating2.setId("h3");

        //genre
        Label genre = new Label("GENRES");
        genre.setId("h4");
        Label genre2 = new Label(genresString);
        genre2.setId("h3");

        //accessibility
        Label acc = new Label("ACESSIBILITY");
        acc.setId("h4");
        String accS = movie.getAccessibility().toString();
        if (accS.equals("0")) accS = "all";
        else accS += "+";
        Label acc2 = new Label(accS);
        acc2.setId("h3");

        //Panes
        titlebox.getChildren().addAll(mtitle);
        titlebox.setPadding(new Insets(15));
        titlebox.setStyle("-fx-background-color: linear-gradient(#90A4AE, #263238)");

        leftBox.getChildren().addAll(poster, rating, rating2, imdb);
        leftBox.setPadding(new Insets(0, 15, 15, 15));
        leftBox.setSpacing(15);

        movieBox.getChildren().add(wv);
        movieBox.setPrefSize(620, 354);

        descBox.getChildren().addAll(mdesc, genre, genre2, acc, acc2);
        descBox.setSpacing(15);

        pane.add(titlebox, 0, 0, 3, 1);
        pane.add(leftBox,  0, 1, 1, 3);
        pane.add(movieBox, 2, 1, 1, 1);
        pane.add(descBox,  2, 2, 1, 1);
        pane.add(showTimeBox, 0, 3, 3, 1);

        pane.setHgap(10);
        pane.setVgap(10);
        pane.setStyle("-fx-background-color: #263238");

        root.setContent(pane);
        root.setFitToWidth(true);
        root.setFitToHeight(true);

        Scene movieViewScene = new Scene(root);
        setScene(movieViewScene);
        movieViewScene.getStylesheets().add("https://fonts.googleapis.com/css?family=Raleway:300,400,700");
        movieViewScene.getStylesheets().add("/css/StyleSheets.css");

        setTitle(movie.getMovieName());
        setModality();
    }

    private Node getShowTimes(List<ShowTime> showTimes) {

        GridPane grid = new GridPane();
        ColumnConstraints column1 = new ColumnConstraints();
        column1.setPercentWidth(50);
        grid.getColumnConstraints().add(column1);

        ColumnConstraints column2 = new ColumnConstraints();
        column2.setPercentWidth(25);
        grid.getColumnConstraints().add(column2);

        ColumnConstraints column3 = new ColumnConstraints();
        column3.setPercentWidth(25);
        grid.getColumnConstraints().add(column3);

        grid.setVgap(20);
        grid.setPadding(new Insets(10));
        grid.getStyleClass().add("main-bg");

        for (int i = 0; i < showTimes.size(); i++) {
            ShowTime st = showTimes.get(i);

            Label lbMovieTitle = new Label(st.getMovieID().getMovieName());
            lbMovieTitle.setId("h3");
            StackPane paneTitle = new StackPane(lbMovieTitle);
            paneTitle.getStyleClass().add("grid-bg");
            grid.add(paneTitle, 0, i);

            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm a");
            String time = dateFormat.format(st.getTime());

            Label lbTime = new Label(time);
            lbTime.setId("h3");
            StackPane paneTime = new StackPane(lbTime);
            paneTime.getStyleClass().add("grid-bg");
            grid.add(paneTime, 1, i);
            
            Button btBuy = new Button("Buy Ticket");
            btBuy.getStyleClass().add("button-white");
            StackPane paneBuy = new StackPane(btBuy);
            paneBuy.getStyleClass().add("grid-bg");

            grid.add(paneBuy, 2, i);
            btBuy.setOnAction(e -> goBuyTicket(st.getId()));
        }

        grid.setMaxHeight(200);
        ScrollPane scroll = new ScrollPane(grid);
        scroll.setStyle("-fx-border-style: none;");
        scroll.getStyleClass().add("main-bg");
        scroll.setHbarPolicy(ScrollBarPolicy.NEVER);
        scroll.setFitToWidth(true);
        scroll.setMaxHeight(200);

        return scroll;
    }

    private void goBuyTicket(int id) {
    	hide();
        new TicketBookingController(id, ProfileController.profileNode);
    }

}
