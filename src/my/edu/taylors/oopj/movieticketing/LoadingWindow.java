package my.edu.taylors.oopj.movieticketing;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class LoadingWindow extends JFrame {
	private static final long serialVersionUID = 1L;

	/**
	 * Basic waiting window when there is a connection to database being made<br>
	 * and JavaFX is launching (that's why it uses swing)
	 */
	public LoadingWindow() {
		super("Ticketing application is starting...");

		ImageIcon loading = new ImageIcon("img/loader.gif");
		String message = " The application is starting. Wait a moment please.";
		add(new JLabel(message, loading, JLabel.CENTER));

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		setSize(380, 150);
		setLocationRelativeTo(null);
		setVisible(true);
	}

}
