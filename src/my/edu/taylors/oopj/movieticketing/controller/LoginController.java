package my.edu.taylors.oopj.movieticketing.controller;

import java.util.Date;

import my.edu.taylors.oopj.movieticketing.Router;
import my.edu.taylors.oopj.movieticketing.entity.Customer;
import my.edu.taylors.oopj.movieticketing.service.LoginService;
import my.edu.taylors.oopj.movieticketing.service.impl.LoginServiceImpl;
import my.edu.taylors.oopj.movieticketing.view.LoginView;
import my.edu.taylors.oopj.movieticketing.view.dto.CreateCustomerDto;
import my.edu.taylors.oopj.movieticketing.view.listener.LoginListener;

public class LoginController extends AbstractController implements LoginListener {

	private LoginService loginService;

	public LoginController() {
		view = new LoginView(this);
		loginService = new LoginServiceImpl();
	}

	@Override
	public void customerCreated(CreateCustomerDto event) {
		Customer customer =
				new Customer(event.getFirstName(), event.getLastName(), event.getEmail(),
				event.getPassword(), event.getPhone(), new Date());
		
		if (loginService.isCustomerEmailDuplicate(customer)) {

			((LoginView)view).creatingAccountFailed();
			
		} else {

			loginService.insertCustomer(new Customer(
					event.getFirstName(), event.getLastName(), event.getEmail(),
					event.getPassword(), event.getPhone(), new Date()));

			((LoginView)view).creatingAccountSuccessful();
		}
		
	}

	@Override
	public void login(String email, String password) {
		if (loginService.authenticate(email, password)) {
			Customer customer = loginService.getCustomerByEmail(email);
			Router.getInstance().navigate(new HomeController(customer));
		} else {
			((LoginView)view).loginFailed();
		}
	}

}
