package my.edu.taylors.oopj.movieticketing.entity;

import java.util.Date;

import my.edu.taylors.oopj.movieticketing.db.AbstractEntity;
import my.edu.taylors.oopj.movieticketing.db.annotation.NotNull;

public class Movie extends AbstractEntity {

	@NotNull
	private String movieName;
	@NotNull
	private int movieRating;
	@NotNull
	private String description;
	@NotNull
	private Accessibility accessibility;
	@NotNull
	private Date releaseDate;
	private String imdb;
	private String img;
	private boolean isHot;
	@NotNull
	private String trailer;

	public Movie(String movieName, int movieRating, String description, Accessibility accessibility, Date releaseDate,
			String imdb, String img, boolean isHot, String trailer) {
		super();
		this.movieName = movieName;
		this.movieRating = movieRating;
		this.description = description;
		this.accessibility = accessibility;
		this.releaseDate = releaseDate;
		this.imdb = imdb;
		this.img = img;
		this.isHot = isHot;
		this.trailer = trailer;
	}

	public Movie(int id, String movieName, int movieRating, String description, Accessibility accessibility,
			Date releaseDate, String imdb, String img, boolean isHot, String trailer) {
		super(id);
		this.movieName = movieName;
		this.movieRating = movieRating;
		this.description = description;
		this.accessibility = accessibility;
		this.releaseDate = releaseDate;
		this.imdb = imdb;
		this.img = img;
		this.isHot = isHot;
		this.trailer = trailer;
	}

	public String getMovieName() {
		return movieName;
	}

	public void setMovieName(String movieName) {
		this.movieName = movieName;
	}

	public int getMovieRating() {
		return movieRating;
	}

	public void setMovieRating(int movieRating) {
		this.movieRating = movieRating;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Accessibility getAccessibility() {
		return accessibility;
	}

	public void setAccessibility(Accessibility accessibility) {
		this.accessibility = accessibility;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	public String getImdb() {
		return imdb;
	}

	public void setImdb(String imdb) {
		this.imdb = imdb;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public boolean isHot() {
		return isHot;
	}

	public void setHot(boolean isHot) {
		this.isHot = isHot;
	}

	public String getTrailer() {
		return trailer;
	}

	public void setTrailer(String trailer) {
		this.trailer = trailer;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((accessibility == null) ? 0 : accessibility.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((imdb == null) ? 0 : imdb.hashCode());
		result = prime * result + ((img == null) ? 0 : img.hashCode());
		result = prime * result + (isHot ? 1231 : 1237);
		result = prime * result + ((movieName == null) ? 0 : movieName.hashCode());
		result = prime * result + movieRating;
		result = prime * result + ((releaseDate == null) ? 0 : releaseDate.hashCode());
		result = prime * result + ((trailer == null) ? 0 : trailer.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Movie other = (Movie) obj;
		if (accessibility != other.accessibility)
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (imdb == null) {
			if (other.imdb != null)
				return false;
		} else if (!imdb.equals(other.imdb))
			return false;
		if (img == null) {
			if (other.img != null)
				return false;
		} else if (!img.equals(other.img))
			return false;
		if (isHot != other.isHot)
			return false;
		if (movieName == null) {
			if (other.movieName != null)
				return false;
		} else if (!movieName.equals(other.movieName))
			return false;
		if (movieRating != other.movieRating)
			return false;
		if (releaseDate == null) {
			if (other.releaseDate != null)
				return false;
		} else if (!releaseDate.equals(other.releaseDate))
			return false;
		if (trailer == null) {
			if (other.trailer != null)
				return false;
		} else if (!trailer.equals(other.trailer))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Movie [movieName=" + movieName + ", movieRating=" + movieRating + ", description=" + description
				+ ", accessibility=" + accessibility + ", releaseDate=" + releaseDate + ", imdb=" + imdb + ", img="
				+ img + ", isHot=" + isHot + ", trailer=" + trailer + ", id=" + id + "]";
	}

}
