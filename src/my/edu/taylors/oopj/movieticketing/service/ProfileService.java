package my.edu.taylors.oopj.movieticketing.service;

import java.util.List;

import my.edu.taylors.oopj.movieticketing.entity.Customer;
import my.edu.taylors.oopj.movieticketing.entity.Payment;

public interface ProfileService {

	List<Payment> getTicketsByCustomer(Customer customer);

	void updateCustomer(Customer customer);

	void updatePassword(Customer customer, String newPassword);

}
