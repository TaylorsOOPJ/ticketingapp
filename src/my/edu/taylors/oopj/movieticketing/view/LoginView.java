package my.edu.taylors.oopj.movieticketing.view;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import my.edu.taylors.oopj.movieticketing.utils.AlertFactory;
import my.edu.taylors.oopj.movieticketing.view.dto.CreateCustomerDto;
import my.edu.taylors.oopj.movieticketing.view.listener.LoginListener;

public class LoginView extends View {

	private Scene loginScene, signUpScene;

	private LoginListener customerListener;

	// login scene
	private TextField tfUserLogin;
	private PasswordField pfPasswordLogin;
	private Button btnDoLogin, btnGoSignUp, btnDoCreateAccount;

	// sign up scene
	private TextField tfFirstName, tfLastName, tfEmail, tfPhone;
	private PasswordField pfPasswordSign, pfConfirm;

	public LoginView(LoginListener customerListener) { 
		this.customerListener = customerListener;
		setLoginScene();
		setSignUpScene();


		setHeight(700);
		setWidth(550);
		//set default scene
		setScene(loginScene);
		setTitle("JAVA Cinemas");
	}

	private void setLoginScene() {

		VBox vbox = new VBox(15);//Pane: spacing of 15
                StackPane sp = new StackPane();

                ImageView coverphoto = new ImageView(new Image("image/cover.png"));

                coverphoto.setFitHeight(300);
                coverphoto.setFitWidth(560);


                sp.getChildren().addAll(coverphoto);

		//TextFields
		tfUserLogin = new TextField();
		pfPasswordLogin = new PasswordField();

		tfUserLogin.setPromptText("Email");
		tfUserLogin.setPrefHeight(35);

		pfPasswordLogin.setPromptText("Password");
		pfPasswordLogin.setPrefHeight(35);
		
		Label lbNoAccount = new Label("Don't have an account? \n");
		lbNoAccount.setStyle("-fx-text-fill: #ffffff;  -fx-font-family: Raleway;");

		tfUserLogin.setId("tfield");
		pfPasswordLogin.setId("tfield");

		//Buttons
		btnDoLogin = new Button("Login");
		btnGoSignUp = new Button("Sign Up");

		//Button style
		btnDoLogin.setId("buttondark");
		btnGoSignUp.setId("buttonlight");
		btnDoLogin.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		btnGoSignUp.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);

		tfUserLogin.requestFocus();

		//-----Adding nodes to Vbox-------
		vbox.getChildren().addAll(sp, new Label("\n"), tfUserLogin, pfPasswordLogin, btnDoLogin,
				lbNoAccount, btnGoSignUp);

		//-----Vbox pane style------

		vbox.getStyleClass().add("background");
		vbox.setPadding(new Insets(0,25,0,25));
		vbox.setPrefSize(500, 720);
		vbox.setAlignment(Pos.TOP_CENTER);
                
		//Set Scene
		loginScene = new Scene(vbox);

		loginScene.getStylesheets().addAll("https://fonts.googleapis.com/css?family=Raleway:300,400,700;");
		loginScene.getStylesheets().add("/css/StyleSheets.css");

		btnDoLogin.setOnAction(e -> doLogin());

		btnGoSignUp.setOnAction(e -> goSignUp());
	}

	private void setSignUpScene() {

		GridPane grid = new GridPane();
		VBox signUpBox = new VBox();

		//-----Declaring nodes------
		//TextFields
		tfFirstName = new TextField();
		tfLastName = new TextField();
		tfEmail = new TextField();
		tfPhone = new TextField();
		pfPasswordSign = new PasswordField(); //password field for sign up
		pfConfirm = new PasswordField();
		tfFirstName.setPromptText("First Name");
		tfLastName.setPromptText("Last Name");
		tfEmail.setPromptText("Email");
		tfPhone.setPromptText("Phone number");
		pfPasswordSign.setPromptText("Password");
		pfConfirm.setPromptText("Confirm Password");

		tfFirstName.setId("tfield");
		tfLastName.setId("tfield");
		tfEmail.setId("tfield");
		tfPhone.setId("tfield");
		pfPasswordSign.setId("tfield");
		pfConfirm.setId("tfield");

		//Title
		Label signup = new Label("Create an account");
		signup.setId("title");

		//Button
		btnDoCreateAccount = new Button("SIGN UP");
		btnDoCreateAccount.setStyle("-fx-border: 0; -fx-color: #263238; -fx-text-fill: #FFFFFF; "
				+ "-fx-width: 20px; -fx-font-family: Raleway; -fx-font-size: 20px; -fx-font-weight: 400");
		btnDoCreateAccount.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		btnDoCreateAccount.setOnAction(e -> doCreateAccount());

		//------Adding nodes to gridpane-----
		//Title
		grid.add(signup, 0, 0, 2, 1);
		GridPane.setHalignment(signup, HPos.CENTER);
		GridPane.setValignment(signup, VPos.TOP);
		GridPane.setMargin(signup, new Insets(10));
		
                //TextFields
		grid.add(tfFirstName,           0, 1, 1, 1);
		grid.add(tfLastName,            1, 1, 1, 1);
		grid.add(tfEmail,               0, 2, 2, 1);
		grid.add(tfPhone,               0, 3, 2, 1);
		grid.add(pfPasswordSign,        0, 4, 2, 1);
		grid.add(pfConfirm,             0, 5, 2, 1);
		
                //Button
		grid.add(btnDoCreateAccount,    0, 6, 2, 1);
		GridPane.setMargin(btnDoCreateAccount, new Insets(15));
		GridPane.setHalignment(btnDoCreateAccount, HPos.CENTER);
		
                //Gridpane style
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(15));
		grid.getStyleClass().add("background");
		grid.setAlignment(Pos.CENTER);

		signUpBox.getChildren().add(grid);
		signUpBox.setPrefSize(560, 720);
		signUpBox.setStyle("-fx-background-color: #37474F");

		Button btGoToLogin = new Button("Back to login");
		btGoToLogin.setOnAction(e -> goLogin());
		btGoToLogin.setId("buttonlight");

		grid.add(btGoToLogin,    0, 7, 2, 1);
		GridPane.setHalignment(btGoToLogin, HPos.CENTER);

		//Set scene
		signUpScene = new Scene(signUpBox);
		signUpScene.getStylesheets().add("https://fonts.googleapis.com/css?family=Raleway:300,400,700");
		signUpScene.getStylesheets().add("/css/StyleSheets.css");
	}

	private void goSignUp() {
		setScene(signUpScene);
	}

	private void goLogin() {
		setScene(loginScene);
	}
	
	private void doLogin() {
		btnDoLogin.setDisable(true);

		String email = tfUserLogin.getText().trim();
		String password = pfPasswordLogin.getText().trim();
		if (email.equals("") || password.equals("")) {
			loginError("Fill both fields.");
		} else {
			customerListener.login(email, password);
		}
	}

	private void doCreateAccount() {
		String firstName = tfFirstName.getText().trim();
		String lastName = tfLastName.getText().trim();
		String password = pfPasswordSign.getText().trim();
		String passwordConfirm = pfConfirm.getText().trim();
		String email = tfEmail.getText().trim();
		String phoneString = tfPhone.getText().trim();

		if (firstName.equals("") || lastName.equals("") || password.equals("")
				|| email.equals("") || phoneString.equals("")) {

			signUpError("Fill all fields.");

		} else {
			try {
				int phone = Integer.parseInt(phoneString);

				if (password.equals(passwordConfirm)) {
					customerListener.customerCreated(new CreateCustomerDto(firstName, lastName, password, phone, email));
				} else {
					signUpError("Passwords do not match.");
				}
			} catch (NumberFormatException ex) {
				signUpError("Phone must be a number.");
			}
		}
	}

	private void loginError(String error) {
		btnDoLogin.setDisable(false);
		AlertFactory.createAlert(Alert.AlertType.ERROR, "Login error", error);
	}

	private void signUpError(String error) {
		btnDoCreateAccount.setDisable(false);
		AlertFactory.createAlert(Alert.AlertType.ERROR, "Sign up error", error);
	}

	public void loginFailed() {
		loginError("Email or password is wrong.");
	}

	public void creatingAccountFailed() {
		signUpError("Account with this email already exists.");
	}

	public void creatingAccountSuccessful() {
		setScene(loginScene);
		AlertFactory.createAlert(Alert.AlertType.INFORMATION, "Account was successfully created.", "You can login now");
	}

}
