package my.edu.taylors.oopj.movieticketing.controller;

import java.util.Calendar;
import java.util.List;

import javafx.scene.Node;
import my.edu.taylors.oopj.movieticketing.entity.ShowTime;
import my.edu.taylors.oopj.movieticketing.service.ShowTimeService;
import my.edu.taylors.oopj.movieticketing.service.impl.ShowTimeServiceImpl;
import my.edu.taylors.oopj.movieticketing.view.HomeView;
import my.edu.taylors.oopj.movieticketing.view.ProfileNode;
import my.edu.taylors.oopj.movieticketing.view.ShowTimeNode;
import my.edu.taylors.oopj.movieticketing.view.listener.ShowTimeListener;

public class ShowTimeController implements ShowTimeListener {

	private ShowTimeService showTimeService;
	private ShowTimeNode showTimeNode;
	private ProfileNode profileNode;

	public ShowTimeController(ProfileNode profileNode) {
		this.profileNode = profileNode;
		showTimeService = new ShowTimeServiceImpl();
		showTimeNode = new ShowTimeNode(this);
	}

	@Override
	public Node getMainNode() {
		return showTimeNode.getMainNode();
	}

	@Override
	public List<ShowTime> getShowTimesByDay(Calendar cal) {
		return showTimeService.getShowTimesByDay(HomeView.currentTheater, cal);
	}

	@Override
	public void goBuyTicket(int id) {
		new TicketBookingController(id, profileNode);
	}

	@Override
	public void notifyChangeTheater() {
		showTimeNode.update();
	}

}
