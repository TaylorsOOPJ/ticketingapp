package my.edu.taylors.oopj.movieticketing.dao;

import java.util.List;

import my.edu.taylors.oopj.movieticketing.entity.Customer;
import my.edu.taylors.oopj.movieticketing.entity.Payment;

public interface PaymentDao extends IAbstractDao<Payment> {

	List<Payment> getPaymentsByCustomer(Customer customer);

}
