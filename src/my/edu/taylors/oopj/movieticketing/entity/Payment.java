package my.edu.taylors.oopj.movieticketing.entity;

import java.util.Date;

import my.edu.taylors.oopj.movieticketing.db.AbstractEntity;
import my.edu.taylors.oopj.movieticketing.db.annotation.ManyToOne;
import my.edu.taylors.oopj.movieticketing.db.annotation.NotNull;

public class Payment extends AbstractEntity {

	private Date timeOfPayment;
	private int numTicket;
	private float totalAmount;
	@ManyToOne
	@NotNull
	private Customer customerID;
	@ManyToOne
	@NotNull
	private ShowTime showtimeID;
	
	public Payment(Date timeOfPayment, int numTicket, float totalAmount, Customer customerID, ShowTime showtimeID) {
		super();
		this.timeOfPayment = timeOfPayment;
		this.numTicket = numTicket;
		this.totalAmount = totalAmount;
		this.customerID = customerID;
		this.showtimeID = showtimeID;
	}

	public Payment(int id, Date timeOfPayment, int numTicket, float totalAmount, Customer customerID,
			ShowTime showtimeID) {
		super(id);
		this.timeOfPayment = timeOfPayment;
		this.numTicket = numTicket;
		this.totalAmount = totalAmount;
		this.customerID = customerID;
		this.showtimeID = showtimeID;
	}

	public Date getTimeOfPayment() {
		return timeOfPayment;
	}

	public void setTimeOfPayment(Date timeOfPayment) {
		this.timeOfPayment = timeOfPayment;
	}

	public int getNumTicket() {
		return numTicket;
	}

	public void setNumTicket(int numTicket) {
		this.numTicket = numTicket;
	}

	public float getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(float totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Customer getCustomerID() {
		return customerID;
	}

	public void setCustomerID(Customer customerID) {
		this.customerID = customerID;
	}

	public ShowTime getShowtimeID() {
		return showtimeID;
	}

	public void setShowtimeID(ShowTime showtimeID) {
		this.showtimeID = showtimeID;
	}

	@Override
	public String toString() {
		return "Payment [timeOfPayment=" + timeOfPayment + ", numTicket=" + numTicket + ", totalAmount=" + totalAmount
				+ ", customerID=" + customerID + ", showtimeID=" + showtimeID + ", id=" + id + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((customerID == null) ? 0 : customerID.hashCode());
		result = prime * result + numTicket;
		result = prime * result + ((showtimeID == null) ? 0 : showtimeID.hashCode());
		result = prime * result + ((timeOfPayment == null) ? 0 : timeOfPayment.hashCode());
		result = prime * result + Float.floatToIntBits(totalAmount);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Payment other = (Payment) obj;
		if (customerID == null) {
			if (other.customerID != null)
				return false;
		} else if (!customerID.equals(other.customerID))
			return false;
		if (numTicket != other.numTicket)
			return false;
		if (showtimeID == null) {
			if (other.showtimeID != null)
				return false;
		} else if (!showtimeID.equals(other.showtimeID))
			return false;
		if (timeOfPayment == null) {
			if (other.timeOfPayment != null)
				return false;
		} else if (!timeOfPayment.equals(other.timeOfPayment))
			return false;
		if (Float.floatToIntBits(totalAmount) != Float.floatToIntBits(other.totalAmount))
			return false;
		return true;
	}

}
