package my.edu.taylors.oopj.movieticketing.entity;

public class Movie_Genre {

	private Movie movieID;
	private Genre genreID;

	public Movie_Genre(Movie movieID, Genre genreID) {
		this.movieID = movieID;
		this.genreID = genreID;
	}

	public Movie getMovieID() {
		return movieID;
	}

	public void setMovieID(Movie movieID) {
		this.movieID = movieID;
	}

	public Genre getGenreID() {
		return genreID;
	}

	public void setGenreID(Genre genreID) {
		this.genreID = genreID;
	}

	@Override
	public String toString() {
		return "Movie_Genre [movieID=" + movieID + ", genreID=" + genreID + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((genreID == null) ? 0 : genreID.hashCode());
		result = prime * result + ((movieID == null) ? 0 : movieID.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Movie_Genre other = (Movie_Genre) obj;
		if (genreID == null) {
			if (other.genreID != null)
				return false;
		} else if (!genreID.equals(other.genreID))
			return false;
		if (movieID == null) {
			if (other.movieID != null)
				return false;
		} else if (!movieID.equals(other.movieID))
			return false;
		return true;
	}
	

}
