package my.edu.taylors.oopj.movieticketing.service;

import java.util.List;
import my.edu.taylors.oopj.movieticketing.entity.Theater;

/**
 *
 * @author VictorTeh
 */
public interface HomeService {

	List<Theater> getAllTheaters();

}
