package my.edu.taylors.oopj.movieticketing.controller;

import java.util.List;
import javafx.scene.Node;
import my.edu.taylors.oopj.movieticketing.entity.Movie;
import my.edu.taylors.oopj.movieticketing.service.HotsService;
import my.edu.taylors.oopj.movieticketing.service.impl.HotsServiceImpl;
import my.edu.taylors.oopj.movieticketing.view.HotsNode;
import my.edu.taylors.oopj.movieticketing.view.listener.HotsListener;

public class HotsController implements HotsListener {

	private HotsService hotsService;
	private HotsNode hotsNode;

	public HotsController() {
		hotsService = new HotsServiceImpl();
		hotsNode = new HotsNode(this);
	}

	@Override
	public List<Movie> getHotMovies() {
		return hotsService.getHotMovies();
	}

	@Override
	public Node getMainNode() {
		return hotsNode.getMainNode();
	}

}
