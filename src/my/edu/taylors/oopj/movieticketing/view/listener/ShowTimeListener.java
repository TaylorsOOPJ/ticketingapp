package my.edu.taylors.oopj.movieticketing.view.listener;

import java.util.Calendar;
import java.util.List;

import javafx.scene.Node;
import my.edu.taylors.oopj.movieticketing.entity.ShowTime;

public interface ShowTimeListener {

	List<ShowTime> getShowTimesByDay(Calendar cal);

	Node getMainNode();

	void goBuyTicket(int id);

	void notifyChangeTheater();

}
