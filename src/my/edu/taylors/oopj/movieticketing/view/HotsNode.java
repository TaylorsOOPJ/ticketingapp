package my.edu.taylors.oopj.movieticketing.view;

import java.util.List;

import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import my.edu.taylors.oopj.movieticketing.entity.Movie;
import my.edu.taylors.oopj.movieticketing.utils.FlipAnimation;
import my.edu.taylors.oopj.movieticketing.view.listener.HotsListener;

public class HotsNode extends MyNode{

    private HotsListener hotsListener;

    public HotsNode(HotsListener hotsListener) {
        this.hotsListener = hotsListener;
        mainNode = new VBox();
        mainNode.setStyle("-fx-background-color: #263238;");
        renderMovies();
    }

    public void renderMovies () {

        List<Movie> hotMovies = hotsListener.getHotMovies();

        Pane moviesBox;

        if (hotMovies.isEmpty()) {
            moviesBox = new StackPane();
            final Label lbNotFound = new Label("No movies found.");
            lbNotFound.setFont(Font.font("Arial", 25));
            lbNotFound.setMaxWidth(Double.MAX_VALUE);
            lbNotFound.setAlignment(Pos.CENTER);
            moviesBox.getChildren().add(lbNotFound);
        } else {
            moviesBox = new FlowPane(Orientation.HORIZONTAL);

            for (int i = 0; i < hotMovies.size(); i++) {
                Movie movie = hotMovies.get(i);

                FlipAnimation flip = new FlipAnimation();
                moviesBox.getChildren().add(flip.createFlipPane(movie));
            }
        }

        moviesBox.getStyleClass().add("main-bg");
        ScrollPane scrollPane = new ScrollPane(moviesBox);
        scrollPane.setFitToWidth(true);
        scrollPane.setFitToHeight(true);
        ((VBox)mainNode).getChildren().add(scrollPane);
    }

}
