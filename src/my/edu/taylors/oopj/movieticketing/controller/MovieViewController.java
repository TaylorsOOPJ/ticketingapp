package my.edu.taylors.oopj.movieticketing.controller;

import java.util.List;

import my.edu.taylors.oopj.movieticketing.entity.Genre;
import my.edu.taylors.oopj.movieticketing.entity.Movie;
import my.edu.taylors.oopj.movieticketing.entity.ShowTime;
import my.edu.taylors.oopj.movieticketing.service.MovieService;
import my.edu.taylors.oopj.movieticketing.service.impl.MovieServiceImpl;
import my.edu.taylors.oopj.movieticketing.view.MovieView;
import my.edu.taylors.oopj.movieticketing.view.listener.MovieViewListener;

public class MovieViewController extends AbstractController implements MovieViewListener {

    private MovieService movieService;

    public MovieViewController(int movieId){
        movieService = new MovieServiceImpl();
        view = new MovieView(this, movieId);
    }

	@Override
	public Movie getMovie(int id) {
		return movieService.getMovie(id);
	}

	@Override
	public List<Genre> getMovieGenres(int movieId) {
		return movieService.getMovieGenres(movieId);
	}

	@Override
	public List<ShowTime> getShowTimesByMovieAndTheater(int movieId, int currentTheater) {
		return movieService.getShowTimesByMovieAndTheater(movieId, currentTheater);
	}

}
