package my.edu.taylors.oopj.movieticketing.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import my.edu.taylors.oopj.movieticketing.dao.PaymentDao;
import my.edu.taylors.oopj.movieticketing.db.DatabaseConnection;
import my.edu.taylors.oopj.movieticketing.entity.Customer;
import my.edu.taylors.oopj.movieticketing.entity.Movie;
import my.edu.taylors.oopj.movieticketing.entity.Payment;
import my.edu.taylors.oopj.movieticketing.entity.ShowTime;
import my.edu.taylors.oopj.movieticketing.entity.Theater;

public class PaymentDaoImpl extends AbstractDao<Payment> implements PaymentDao {

	public static final String SQL = "p.id, p.timeOfPayment, p.numTicket, p.totalAmount";

	public static Payment getShowTimeFromResult(ResultSet results, ShowTime showTime, Customer customer) throws SQLException {
		int id = results.getInt("p.id");
		Timestamp timeOfPaymentStamp = results.getTimestamp("p.timeOfPayment");
		Date timeOfPayment = new Date(timeOfPaymentStamp.getTime());
		int numTicket = results.getInt("p.numTicket");
		float totalAmount = results.getFloat("p.totalAmount");

		return new Payment(id, timeOfPayment, numTicket, totalAmount, customer, showTime);
	}

	@Override
	public Payment getByKey(int key) {
		List<Payment> payments = find("WHERE p.id = ?", key);
		return payments.size() > 0 ? payments.get(0) : null;
	}

	@Override
	public List<Payment> findAll() {
		return find("");
	}

	@Override
	public List<Payment> getPaymentsByCustomer(Customer customer) {
		return find("WHERE c.id = ? ORDER BY st.time", customer.getId());
	}

	private List<Payment> find(String where, Object... args) {

		try {
			Connection conn = DatabaseConnection.getInstance().getConnection();

			String sql = 
				"SELECT " + PaymentDaoImpl.SQL + " , " + CustomerDaoImpl.SQL + ", " + ShowTimeDaoImpl.SQL + ", " +
							MovieDaoImpl.SQL + ", " + TheaterDaoImpl.SQL + " " +
				"FROM payment p " +
				"INNER JOIN customer c ON p.customerID = c.id " +
				"INNER JOIN showtime st ON p.showtimeID = st.id " +
				"INNER JOIN movie m ON st.movieID = m.id " +
				"INNER JOIN theater t ON st.theaterID = t.id " + where;
			PreparedStatement selectStatement = conn.prepareStatement(sql);
			for (int i = 0; i < args.length; i++) {
				selectStatement.setObject(i+1, args[i]);
			}

			ResultSet results = selectStatement.executeQuery();
			List<Payment> payments = new ArrayList<>();

			while (results.next()) {

				Theater theater = TheaterDaoImpl.getTheaterFromResult(results);
				Movie movie = MovieDaoImpl.getMovieFromResult(results);
				ShowTime showTime = ShowTimeDaoImpl.getShowTimeFromResult(results, movie, theater);
				Customer customer = CustomerDaoImpl.getCustomerFromResult(results);
				Payment payment = getShowTimeFromResult(results, showTime, customer);

				payments.add(payment);
			}

			return payments;
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return new ArrayList<>(0);
	}

}
