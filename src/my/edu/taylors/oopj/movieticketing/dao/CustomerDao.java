package my.edu.taylors.oopj.movieticketing.dao;

import my.edu.taylors.oopj.movieticketing.entity.Customer;

public interface CustomerDao extends IAbstractDao<Customer> {

	Customer getByEmail(String email);

}