package my.edu.taylors.oopj.movieticketing.view;

import javafx.scene.Node;

public abstract class MyNode {

	protected Node mainNode;

	public Node getMainNode() {
		return mainNode;
	}

	public void setMainNode(Node mainNode) {
		this.mainNode = mainNode;
	}

}
