package my.edu.taylors.oopj.movieticketing.service;

import java.util.List;
import my.edu.taylors.oopj.movieticketing.entity.Movie;

public interface HotsService {

	List <Movie> getHotMovies ();

}
