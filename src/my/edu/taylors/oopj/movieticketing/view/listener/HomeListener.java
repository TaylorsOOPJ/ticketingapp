package my.edu.taylors.oopj.movieticketing.view.listener;

import java.util.List;

import my.edu.taylors.oopj.movieticketing.entity.Theater;

public interface HomeListener {

	void logout();

	List<Theater> getAllTheters();

}
