package my.edu.taylors.oopj.movieticketing.view;

import java.util.Calendar;
import java.util.Date;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import my.edu.taylors.oopj.movieticketing.entity.Payment;

public class TicketDetailView extends View {

	private Payment payment;

	public TicketDetailView(Payment payment) {
		this.payment = payment;
		setupGui();
		setModality();
	}

	private void setupGui() {
		final String headingStyle = "-fx-text-fill: #FFFFFF; -fx-font-size: 25px; -fx-font-family: Raleway; -fx-font-weight: 300";
		final String normalStyle  = "-fx-text-fill: #FFFFFF; -fx-font-size: 16px; -fx-font-family: Raleway";

		GridPane ticketBookingPane = new GridPane();

		Label lbMovieName = new Label(payment.getShowtimeID().getMovieID().getMovieName());
		lbMovieName.setStyle(headingStyle);
		ticketBookingPane.add(lbMovieName, 0, 0, 2, 1);

		// number of tickets
		Label lbNumTickets = new Label("Number of tickets ");
		lbNumTickets.setStyle(normalStyle);
		Label lbNumTickets2 = new Label(""+payment.getNumTicket());
		lbNumTickets2.setStyle(normalStyle);
		ticketBookingPane.add(lbNumTickets, 0, 1);
		ticketBookingPane.add(lbNumTickets2, 1, 1);

		// price
		Label lbPrice = new Label("Total price ");
		lbPrice.setStyle(normalStyle);
		Label lbPrice2 = new Label("RM"+payment.getTotalAmount());
		lbPrice2.setStyle(normalStyle);
		ticketBookingPane.add(lbPrice, 0, 2);
		ticketBookingPane.add(lbPrice2, 1, 2);

		// date & time of purchase
		Label lbDateTP = new Label("Time of purchase ");
		lbDateTP.setStyle(normalStyle);
		Label lbDateTP2 = new Label(getDate(payment.getTimeOfPayment()));
		lbDateTP2.setStyle(normalStyle);
		ticketBookingPane.add(lbDateTP, 0, 3);
		ticketBookingPane.add(lbDateTP2, 1, 3);

		// theater
		Label lbTheater = new Label("Theater ");
		lbTheater.setStyle(normalStyle);
		Label lbTheater2 = new Label(payment.getShowtimeID().getTheaterID().getTheaterName());
		lbTheater2.setStyle(normalStyle);
		ticketBookingPane.add(lbTheater, 0, 4);
		ticketBookingPane.add(lbTheater2, 1, 4);

		// date & time of showtime
		Label lbDateST = new Label("Time of showtime ");
		lbDateST.setStyle(normalStyle);
		Label lbDateST2 = new Label(getDate(payment.getShowtimeID().getTime()));
		lbDateST2.setStyle(normalStyle);
		ticketBookingPane.add(lbDateST, 0, 5);
		ticketBookingPane.add(lbDateST2, 1, 5);

		// Styling Pane
		ticketBookingPane.setStyle("-fx-background-color: #37474F");
		ticketBookingPane.setPadding(new Insets(15));
		ticketBookingPane.setHgap(15);
		ticketBookingPane.setVgap(15);

		// Set scene
		Scene ticketDeatilScene = new Scene(ticketBookingPane);
		ticketDeatilScene.getStylesheets().add("https://fonts.googleapis.com/css?family=Raleway:300,400,700");
		setScene(ticketDeatilScene);
		setTitle("Detail of ticket");
	}

	private String getDate(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);

		int year = cal.get(Calendar.YEAR);

		int monthInt = cal.get(Calendar.MONTH)+1;
		String month = (monthInt < 10 ? "0" : "") + monthInt;

		int dayInt = cal.get(Calendar.DAY_OF_MONTH);
		String day = (dayInt < 10 ? "0" : "") + dayInt;

		int hour = cal.get(Calendar.HOUR);
		int minute = cal.get(Calendar.MINUTE);
		String AM_PM = cal.get(Calendar.AM_PM) == Calendar.AM ? "am" : "pm";
		if (hour == 0) hour = 12;
		String time = day + "/" + month + "/" + year + " " +hour + "." + minute + " " + AM_PM;

		return time;
	}

}
