package my.edu.taylors.oopj.movieticketing.service.impl;

import java.util.List;

import my.edu.taylors.oopj.movieticketing.dao.GenreDao;
import my.edu.taylors.oopj.movieticketing.dao.MovieDao;
import my.edu.taylors.oopj.movieticketing.dao.ShowTimeDao;
import my.edu.taylors.oopj.movieticketing.dao.impl.GenreDaoImpl;
import my.edu.taylors.oopj.movieticketing.dao.impl.MovieDaoImpl;
import my.edu.taylors.oopj.movieticketing.dao.impl.ShowTimeDaoImpl;
import my.edu.taylors.oopj.movieticketing.entity.Genre;
import my.edu.taylors.oopj.movieticketing.entity.Movie;
import my.edu.taylors.oopj.movieticketing.entity.ShowTime;
import my.edu.taylors.oopj.movieticketing.service.MovieService;

public class MovieServiceImpl implements MovieService{

	private MovieDao movieDao;
	private GenreDao genreDao;
	private ShowTimeDao showTimeDao;

	public MovieServiceImpl() {
		movieDao = new MovieDaoImpl();
		genreDao = new GenreDaoImpl();
		showTimeDao = new ShowTimeDaoImpl();
	}

	@Override
	public Movie getMovie(int id) {
		return movieDao.getByKey(id);
	}

	@Override
	public List<Genre> getMovieGenres(int movieId) {
		return genreDao.getMovieGenres(movieId);
	}

	@Override
	public List<ShowTime> getShowTimesByMovieAndTheater(int movieId, int theaterId) {
		return showTimeDao.findByMovieAndTheatre(movieId, theaterId);
	}

}
