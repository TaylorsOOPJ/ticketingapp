package my.edu.taylors.oopj.movieticketing.entity;

import java.util.Date;

import my.edu.taylors.oopj.movieticketing.db.AbstractEntity;
import my.edu.taylors.oopj.movieticketing.db.annotation.ManyToOne;
import my.edu.taylors.oopj.movieticketing.db.annotation.NotNull;

public class ShowTime extends AbstractEntity {

	private Date time;
	private float price;
	private int ticketsAvailable;
	@ManyToOne
	@NotNull
	private Movie movieID;
	@ManyToOne
	@NotNull
	private Theater theaterID;

	public ShowTime(Date time, float price, int ticketsAvailable, Movie movieID, Theater theaterID) {
		super();
		this.time = time;
		this.price = price;
		this.ticketsAvailable = ticketsAvailable;
		this.movieID = movieID;
		this.theaterID = theaterID;
	}

	public ShowTime(int id, Date time, float price, int ticketsAvailable, Movie movieID, Theater theaterID) {
		super(id);
		this.time = time;
		this.price = price;
		this.ticketsAvailable = ticketsAvailable;
		this.movieID = movieID;
		this.theaterID = theaterID;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public int getTicketsAvailable() {
		return ticketsAvailable;
	}

	public void setTicketsAvailable(int ticketsAvailable) {
		this.ticketsAvailable = ticketsAvailable;
	}

	public Movie getMovieID() {
		return movieID;
	}

	public void setMovieID(Movie movieID) {
		this.movieID = movieID;
	}

	public Theater getTheaterID() {
		return theaterID;
	}

	public void setTheaterID(Theater theaterID) {
		this.theaterID = theaterID;
	}

	@Override
	public String toString() {
		return "ShowTime [time=" + time + ", price=" + price + ", ticketsAvailable=" + ticketsAvailable + ", movieID="
				+ movieID + ", theaterID=" + theaterID + ", id=" + id + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((movieID == null) ? 0 : movieID.hashCode());
		result = prime * result + Float.floatToIntBits(price);
		result = prime * result + ((theaterID == null) ? 0 : theaterID.hashCode());
		result = prime * result + ticketsAvailable;
		result = prime * result + ((time == null) ? 0 : time.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ShowTime other = (ShowTime) obj;
		if (movieID == null) {
			if (other.movieID != null)
				return false;
		} else if (!movieID.equals(other.movieID))
			return false;
		if (Float.floatToIntBits(price) != Float.floatToIntBits(other.price))
			return false;
		if (theaterID == null) {
			if (other.theaterID != null)
				return false;
		} else if (!theaterID.equals(other.theaterID))
			return false;
		if (ticketsAvailable != other.ticketsAvailable)
			return false;
		if (time == null) {
			if (other.time != null)
				return false;
		} else if (!time.equals(other.time))
			return false;
		return true;
	}

}
