package my.edu.taylors.oopj.movieticketing.dao;

import my.edu.taylors.oopj.movieticketing.entity.Theater;

public interface TheaterDao extends IAbstractDao<Theater> {

}
