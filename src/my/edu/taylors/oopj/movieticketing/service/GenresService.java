package my.edu.taylors.oopj.movieticketing.service;

import java.util.List;

import my.edu.taylors.oopj.movieticketing.entity.Genre;
import my.edu.taylors.oopj.movieticketing.entity.Movie;

public interface GenresService {

	List<Movie> getAllMovies();

	List<Genre> getAllGenres();

	List<Movie> getMoviesByGenre(int selectedGenre);

}
