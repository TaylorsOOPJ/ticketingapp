package my.edu.taylors.oopj.movieticketing;

import javafx.stage.Stage;
import my.edu.taylors.oopj.movieticketing.controller.AbstractController;

/**
 * Manages having only one visible stage at a time
 */
public class Router {

	private static final Router instance = new Router();

	private Router() { }

	private Stage stage;

	public static Router getInstance() {
		return instance;
	}

	/**
	 * Main navigation method<br>
	 * Every controller has a view which extends JavaFX Stage
	 * @param controller
	 */
	public void navigate(AbstractController controller) {
		setStage(controller.getView());
	}

	public void setStage(Stage stage) {
		if (this.stage != null) {
			unsetStage();
		}
		this.stage = stage;
		this.stage.show();
	}

	public void unsetStage() {
		stage.hide();
		stage = null;
	}

}
