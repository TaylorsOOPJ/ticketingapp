package my.edu.taylors.oopj.movieticketing.dao;

import java.util.List;

public interface IAbstractDao<T> {

	T getByKey(int key);

	void insert(T entity);

	void update(T entity);

	void delete(T entity);
	
	List<T> findAll();

}
