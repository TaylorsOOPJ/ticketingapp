package my.edu.taylors.oopj.movieticketing.dao;

import java.util.List;

import my.edu.taylors.oopj.movieticketing.entity.Movie;

public interface MovieDao extends IAbstractDao<Movie> {

	List<Movie> getByName(String name);

	List<Movie> findAllByGenreId(int selectedGenre);

	List<Movie> findAllByHot();

}
