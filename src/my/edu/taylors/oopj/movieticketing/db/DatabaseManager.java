package my.edu.taylors.oopj.movieticketing.db;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Database manager class for performing basic operations<br>
 * This class represents only public access to this package
 */
public class DatabaseManager {

	private StatementBuilder statementBuilder;

	public DatabaseManager() {
		statementBuilder = new StatementBuilder();
	}

	/**
	 * Performs insert operation on given entity<br>
	 * If operation is successful, assigns new id to this entity
	 * @param entity
	 */
	public void insert(Object entity) {

		AbstractEntity aEntity = checkEntity(entity, true);
		String tableName = statementBuilder.getTableName(aEntity);

		try {
			PreparedStatement stmt = statementBuilder.getInsertStmt(aEntity);
			int affectedRows = stmt.executeUpdate();

			if (affectedRows == 0) {
				throw new SQLException("Creating user failed, no rows affected.");
			}

			try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
				if (generatedKeys.next()) {
					aEntity.setId(generatedKeys.getInt(1));
				} else {
					throw new SQLException("Inserting " + tableName + " failed.");
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Performs update operation on given entity<br>
	 * Entity must have set id
	 * @param entity
	 */
	public void update(Object entity) {

		AbstractEntity aEntity = checkEntity(entity, false);
		String tableName = statementBuilder.getTableName(aEntity);

		try {
			PreparedStatement stmt = statementBuilder.getUpdateStmt(aEntity);
			int affectedRows = stmt.executeUpdate();

			if (affectedRows == 0) {
				throw new SQLException("Updating " + tableName + " failed, no rows affected.");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}


	/**
	 * Performs delete operation on given entity<br>
	 * Entity must have set id<br>
	 * If operation is successful, entity is set to null
	 * @param entity
	 */
	public void delete(Object entity) {

		AbstractEntity aEntity = checkEntity(entity, false);
		String tableName = statementBuilder.getTableName(aEntity);

		try {
			PreparedStatement stmt = statementBuilder.getDeleteStmt(aEntity);
			int affectedRows = stmt.executeUpdate();
			if (affectedRows == 0) {
				throw new SQLException("Deleting " + tableName + " failed, no rows affected.");
			}
			entity = null;
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	

	
	/**
	 * Check entity if it passes all requirements<br>
	 * - is not null<br>
	 * - extends AbstractEntity<br>
	 * - has set id in case of update and delete
	 * @param entity
	 * @param isNew true for inserting, false otherwise
	 * @return casted entity to AbstractEntity
	 */
	private AbstractEntity checkEntity(Object entity, boolean isNew) {
		if (entity == null) {
			throw new NullPointerException("Passed entity is null.");
		}

		final AbstractEntity aEntity;
		try {
			aEntity = (AbstractEntity) entity;
		} catch (ClassCastException e) {
			throw new ClassCastException("Entity " + statementBuilder.getFullClassName(entity) + " must extend AbstractEntity class");
		}

		if (!isNew && aEntity.getId() == 0) {
			return null;
			// TODO Unpersisted exception
		}

		return aEntity;
	}
}
