package my.edu.taylors.oopj.movieticketing.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Class for maintaining connection to database<br>
 * Implemented as Singleton
 */
public class DatabaseConnection {

	private static final DatabaseConnection instance = new DatabaseConnection();

	private Connection con;

	private DatabaseConnection() { }

	/**
	 * Get instance of this singleton
	 * @return
	 */
	public static DatabaseConnection getInstance() {
		return instance;
	}
	
	/**
	 * Get connection to database
	 * @return {@link Connection}
	 */
	public Connection getConnection() {
		return con;
	}

	/**
	 * Create connection to database
	 * @throws Exception
	 */
	public void connect() {
		if (con != null) {
			return;
		}

		try {
			Class.forName("com.mysql.jdbc.Driver");

			String host = "jdbc:mysql://sql6.freemysqlhosting.net:3306/sql6140956?characterEncoding=utf8";
			String username = "sql6140956";
			String password = "FmRtyI58tt";
			con = DriverManager.getConnection(host, username, password);
			System.out.println("Database connected.");

		} catch (ClassNotFoundException e) {
			System.err.println("MySQL driver was not found. Exiting application.");
			e.printStackTrace();
			System.exit(0);

		} catch (SQLException e) {
			System.err.println("An error occurred when trying to make a connection to database. Exiting appliciton.");
			e.printStackTrace();
			System.exit(0);
		}
	}

	public void disconnect() {
		if (con != null) {
			try {
				con.close();
			} catch (SQLException e) {
				System.out.println("Can't close connection");
			}
		}

		con = null;
	}

}
