package my.edu.taylors.oopj.movieticketing.view.listener;

import my.edu.taylors.oopj.movieticketing.entity.Payment;
import my.edu.taylors.oopj.movieticketing.entity.ShowTime;

public interface TicketBookingListener {

	ShowTime getShowTime(int showTimeId);

	void savePayment(Payment payment);

	void updateTicketsView();  

}
