package my.edu.taylors.oopj.movieticketing.service.impl;

import java.util.Calendar;
import java.util.List;

import my.edu.taylors.oopj.movieticketing.dao.ShowTimeDao;
import my.edu.taylors.oopj.movieticketing.dao.TheaterDao;
import my.edu.taylors.oopj.movieticketing.dao.impl.ShowTimeDaoImpl;
import my.edu.taylors.oopj.movieticketing.dao.impl.TheaterDaoImpl;
import my.edu.taylors.oopj.movieticketing.entity.ShowTime;
import my.edu.taylors.oopj.movieticketing.entity.Theater;
import my.edu.taylors.oopj.movieticketing.service.ShowTimeService;

public class ShowTimeServiceImpl implements ShowTimeService {

	private ShowTimeDao showTimeDao;
	private TheaterDao theaterDao;

	public ShowTimeServiceImpl() {
		showTimeDao = new ShowTimeDaoImpl();
		theaterDao = new TheaterDaoImpl();
	}

	@Override
	public Theater getTheaterById(int theaterId) {
		return theaterDao.getByKey(theaterId);
	}

	@Override
	public List<ShowTime> getShowTimesByDay(int theaterId, Calendar cal) {
		return showTimeDao.findByDay(theaterId, cal);
	}

}
