package my.edu.taylors.oopj.movieticketing.controller;

import java.util.List;

import javafx.scene.Node;
import my.edu.taylors.oopj.movieticketing.entity.Customer;
import my.edu.taylors.oopj.movieticketing.entity.Payment;
import my.edu.taylors.oopj.movieticketing.service.ProfileService;
import my.edu.taylors.oopj.movieticketing.service.impl.ProfileServiceImpl;
import my.edu.taylors.oopj.movieticketing.view.ProfileNode;
import my.edu.taylors.oopj.movieticketing.view.dto.UpdateCustomerDto;
import my.edu.taylors.oopj.movieticketing.view.listener.ProfileListener;

public class ProfileController implements ProfileListener {

	private ProfileService profileService;
	public static ProfileNode profileNode;
	private Customer customer;
	
	public ProfileController(Customer customer) {
		this.customer = customer;
		profileService = new ProfileServiceImpl();
		profileNode = new ProfileNode(this);
	}

	@Override
	public Customer getLoggedCustomer() {
		return customer;
	}

	@Override
	public List<Payment> getTicketsByCustomer(Customer customer) {
		return profileService.getTicketsByCustomer(customer);
	}

	@Override
	public Node getMainNode() {
		return profileNode.getMainNode();
	}

	@Override
	public void updateCustomer(UpdateCustomerDto updateCustomerDto) {
		customer.setFirstName(updateCustomerDto.getFirstName());
		customer.setLastName(updateCustomerDto.getLastName());
		customer.setEmail(updateCustomerDto.getEmail());
		customer.setPhone(updateCustomerDto.getPhone());
		profileService.updateCustomer(customer);
		profileNode.customerUpdateSuccessful();
	}

	@Override
	public void updatePassword(String newPassword) {
		profileService.updatePassword(customer, newPassword);
		profileNode.customerUpdateSuccessful();
	}

	@Override
	public ProfileNode getProfileNode() {
		return profileNode;
	}
}
