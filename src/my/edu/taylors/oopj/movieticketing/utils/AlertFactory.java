package my.edu.taylors.oopj.movieticketing.utils;

import javafx.scene.control.Alert;
import javafx.stage.StageStyle;

public class AlertFactory {

	public static void createAlert(Alert.AlertType type, String title, String message) {
		Alert alert = new Alert(type);
		alert.initStyle(StageStyle.UTILITY);
		alert.setTitle("Information");
		alert.setHeaderText(title);
		alert.setContentText(message);

		alert.showAndWait();
	}

}
