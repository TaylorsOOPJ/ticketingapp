package my.edu.taylors.oopj.movieticketing.dao.impl;

import java.util.List;

import my.edu.taylors.oopj.movieticketing.dao.IAbstractDao;
import my.edu.taylors.oopj.movieticketing.db.DatabaseManager;

public abstract class AbstractDao<T> implements IAbstractDao<T> {

	private final DatabaseManager dbManager;

	public AbstractDao() {
		dbManager = new DatabaseManager();
	}

	/**
	 * Return entity by its primary key
	 * @param key primary key
	 * @return entity
	 */
	@Override
	public abstract T getByKey(int key);

	/**
	 * Save new entity to database
	 * @param entity
	 */
	@Override
	public void insert(T entity) {
		dbManager.insert(entity);
	}

	/**
	 * Update entity in database
	 * @param entity
	 */
	@Override
	public void update(T entity) {
		dbManager.update(entity);
	}

	/**
	 * Delete entity from database
	 * @param entity
	 */
	@Override
	public void delete(T entity) {
		dbManager.delete(entity);
	}

	@Override
	public abstract List<T> findAll();

}
