package my.edu.taylors.oopj.movieticketing.service.impl;

import my.edu.taylors.oopj.movieticketing.dao.PaymentDao;
import my.edu.taylors.oopj.movieticketing.dao.ShowTimeDao;
import my.edu.taylors.oopj.movieticketing.dao.impl.PaymentDaoImpl;
import my.edu.taylors.oopj.movieticketing.dao.impl.ShowTimeDaoImpl;
import my.edu.taylors.oopj.movieticketing.entity.Payment;
import my.edu.taylors.oopj.movieticketing.entity.ShowTime;
import my.edu.taylors.oopj.movieticketing.service.TicketBookingService;

/**
 * @author Johnny Chan
 * @author Milan Kostak
 */
public class TicketBookingServiceImpl implements TicketBookingService {

	private ShowTimeDao showTimeDao;
	private PaymentDao paymentDao;

	public TicketBookingServiceImpl() {
		showTimeDao = new ShowTimeDaoImpl();
		paymentDao = new PaymentDaoImpl();
	}

	@Override
	public ShowTime getShowTime(int showTimeId) {
		return showTimeDao.getByKey(showTimeId);
	}

	@Override
	public void savePayment(Payment payment) {
		paymentDao.insert(payment);
		// lower available tickets
		ShowTime st = showTimeDao.getByKey(payment.getId());
		st.setTicketsAvailable(st.getTicketsAvailable() - payment.getNumTicket());
		showTimeDao.update(st);
	}

}
