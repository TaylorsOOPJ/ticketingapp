package my.edu.taylors.oopj.movieticketing.view;

import java.util.List;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import my.edu.taylors.oopj.movieticketing.controller.GenresController;
import my.edu.taylors.oopj.movieticketing.controller.HotsController;
import my.edu.taylors.oopj.movieticketing.controller.ProfileController;
import my.edu.taylors.oopj.movieticketing.controller.ShowTimeController;
import my.edu.taylors.oopj.movieticketing.entity.Customer;
import my.edu.taylors.oopj.movieticketing.entity.Theater;
import my.edu.taylors.oopj.movieticketing.view.listener.GenresListener;
import my.edu.taylors.oopj.movieticketing.view.listener.HomeListener;
import my.edu.taylors.oopj.movieticketing.view.listener.HotsListener;
import my.edu.taylors.oopj.movieticketing.view.listener.ProfileListener;
import my.edu.taylors.oopj.movieticketing.view.listener.ShowTimeListener;

/**
 *
 * @author VictorTeh
 */
public class HomeView extends View {

	private HomeListener homeListener;
	private HotsListener hotsListener;
	private ShowTimeListener showTimeListener;
	private GenresListener genresListener;
	private ProfileListener profileListener;

	private ComboBox<String> cbTheaters;
	private List<Theater> theaters;

	public static Customer customer;
	public static Stage homeStage;
	public static int currentTheater;

    public HomeView(HomeListener homeListener, Customer customer) {
    	HomeView.homeStage = this;
        HomeView.customer = customer;
        HomeView.currentTheater = 1;
        this.homeListener = homeListener;

        setupListeners();
        setupGui();
    }

    private void setupListeners() {
        hotsListener = new HotsController();
        profileListener = new ProfileController(customer);
        showTimeListener = new ShowTimeController(profileListener.getProfileNode());
        genresListener = new GenresController();
	}

	private void setupGui() {
        VBox vBox = new VBox();
        vBox.setStyle("-fx-background-color: #37474F;");
        //vBox.getStyleClass().add("background");
        Scene homeScene = new Scene(vBox, 1280, 720);
        ImageView logo = new ImageView(new Image("image/logo.png"));
        logo.setFitWidth(120);
        logo.setPreserveRatio(true);

        //combobox of theater
        theaters = homeListener.getAllTheters();
        cbTheaters = new ComboBox<>();

        cbTheaters.setStyle("-fx-text-fill: #263238; -fx-background-color: #ECEFF1; -fx-font-family: Raleway; -fx-font-size: 18px; -fx-font-weight: 400;");
        for (Theater theater : theaters) {
            cbTheaters.getItems().add(theater.getTheaterName());
        }
        
        cbTheaters.setValue(theaters.get(0).getTheaterName());
        cbTheaters.setOnAction(e -> changeTheater());

        GridPane gpMenuBar = new GridPane();

        Button btLogOut = new Button("Log Out");
        btLogOut.setOnAction(e -> homeListener.logout());
        btLogOut.setStyle("-fx-color: #263238; -fx-text-fill: #FFFFFF; -fx-font-family: Raleway; -fx-font-size: 16px; -fx-font-weight: 400;");

        Label lbName = new Label(customer.getFirstName() + " " + customer.getLastName());
        lbName.setStyle("-fx-text-fill: #ffffff; -fx-font-family: Raleway; -fx-font-size: 15px; -fx-font-weight: 400;");
        gpMenuBar.add(lbName, 0, 0);
        gpMenuBar.add(btLogOut, 0, 1);
        gpMenuBar.getStyleClass().add("background");

        BorderPane borderPane = new BorderPane();
        borderPane.setLeft(logo);
        borderPane.setCenter(cbTheaters);
        borderPane.setRight(gpMenuBar);
        borderPane.setPadding(new Insets(10, 10, 10, 10));

        borderPane.getStyleClass().add("background");

        //WHAT'S HOT
        Tab tWhatsHot = new Tab("WHAT'S HOT");
        tWhatsHot.setContent(hotsListener.getMainNode());

        //SHOWTIME
        Tab tShowTime = new Tab("SHOWTIME");
        tShowTime.setContent(showTimeListener.getMainNode());


        //LIST OF ALL
        Tab tListOfAll = new Tab("LIST OF ALL");
        tListOfAll.setContent(genresListener.getMainNode());


        //My account
        Tab tMyAccount = new Tab("MY ACCOUNT");
        tMyAccount.setContent(profileListener.getMainNode());


        TabPane tabPane = new TabPane(tWhatsHot, tShowTime, tListOfAll, tMyAccount);
        tabPane.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);

        vBox.getChildren().addAll(borderPane, tabPane);

        homeScene.getStylesheets().add("/css/StyleSheets.css"); // Load the stylesheet
        setTitle("Welcome to OOP Cinema");
        setScene(homeScene);

    }

	private void changeTheater() {
		int index = cbTheaters.getSelectionModel().getSelectedIndex();
		currentTheater = theaters.get(index).getId();
		showTimeListener.notifyChangeTheater();
	}

}
