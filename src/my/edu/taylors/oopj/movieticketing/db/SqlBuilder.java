package my.edu.taylors.oopj.movieticketing.db;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * The most down class, takes name of fields and builds SQL query
 */
class SqlBuilder {

	SqlBuilder() { }

	/**
	 * Build INSERT query
	 * @param tableName
	 * @param sqlValues
	 * @return INSERT query
	 */
	String getInsertSQL(String tableName, SqlValues sqlValues) {
		
		String[][] strings = getColumnsAndValuesAsStringArrays(sqlValues);
		String[] columns = strings[0];
		String[] params = strings[1];

		String sqlColumns = String.join(", ", columns);
		String sqlParams = String.join(", ", params);

		String sql = String.format("INSERT INTO " + tableName + " (%1$s) VALUES (%2$s)", sqlColumns, sqlParams);
		System.out.println(sql);

		return sql;
	}

	/**
	 * Build UPDATE query
	 * @param tableName
	 * @param sqlValues
	 * @return UPDATE query
	 */
	String getUpdateSQL(String tableName, SqlValues sqlValues) {

		String[][] strings = getColumnsAndValuesAsStringArrays(sqlValues);
		String[] columns = strings[0];

		String sqlColumns = String.join(" = ?, ", columns);
		sqlColumns += " = ?";

		String sql = String.format("UPDATE " + tableName + " SET %1$s WHERE id = ?", sqlColumns);
		System.out.println(sql);

		return sql;
	}

	/**
	 * Read back fields names and values and fill an array with them<br>
	 * Return this array and let insert and update operations join them in their specific way
	 * @param sqlValues SqlValues
	 * @return String[][]
	 */
	private String[][] getColumnsAndValuesAsStringArrays(SqlValues sqlValues) {

		List<String> columnsList = new ArrayList<>();
		List<String> paramsList = new ArrayList<>();
		
		Map<String, String> stringMap = sqlValues.getStringVals();
		Map<String, Integer> intMap = sqlValues.getIntVals();
		Map<String, Float> floatMap = sqlValues.getFloatVals();
		Map<String, Double> doubleMap = sqlValues.getDoubleVals();
		Map<String, Timestamp> dateMap = sqlValues.getDateVals();

		// get strings
		for (Map.Entry<String, String> entry : stringMap.entrySet()) {
			columnsList.add(entry.getKey());
			paramsList.add("?");
		}

		// get integers
		for (Map.Entry<String, Integer> entry : intMap.entrySet()) {
			columnsList.add(entry.getKey());
			paramsList.add("?");
		}

		// get floats
		for (Map.Entry<String, Float> entry : floatMap.entrySet()) {
			columnsList.add(entry.getKey());
			paramsList.add("?");
		}

		// get doubles
		for (Map.Entry<String, Double> entry : doubleMap.entrySet()) {
			columnsList.add(entry.getKey());
			paramsList.add("?");
		}

		// get dates
		for (Map.Entry<String, Timestamp> entry : dateMap.entrySet()) {
			columnsList.add(entry.getKey());
			paramsList.add("?");
		}
		
		// finish
		String[] columnsArray = columnsList.toArray(new String[0]);
		String[] paramsArray = paramsList.toArray(new String[0]);
		String[][] finalArray = new String[][] {columnsArray, paramsArray}; 
		return finalArray;
	}

}
