package my.edu.taylors.oopj.movieticketing.controller;

import my.edu.taylors.oopj.movieticketing.entity.Payment;
import my.edu.taylors.oopj.movieticketing.service.TicketDetailService;
import my.edu.taylors.oopj.movieticketing.service.impl.TicketDetailServiceImpl;
import my.edu.taylors.oopj.movieticketing.view.TicketDetailView;

public class TicketDetailController extends AbstractController {

	public TicketDetailController(int paymentId) {
		TicketDetailService ticketDetailService = new TicketDetailServiceImpl();
		Payment payment = ticketDetailService.getPaymentById(paymentId);
		view = new TicketDetailView(payment);
	}

}
