package my.edu.taylors.oopj.movieticketing.db.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.ElementType;

/**
 * Serves as marker for fields inside entities which are not just primitive fields, but contain another entity<br>
 * When building SQL statements it marks not to take the field directly, but id of marked entity
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface ManyToOne {

}
