package my.edu.taylors.oopj.movieticketing.view.listener;

import java.util.List;

import javafx.scene.Node;
import my.edu.taylors.oopj.movieticketing.entity.Genre;
import my.edu.taylors.oopj.movieticketing.entity.Movie;

public interface GenresListener {

	List<Movie> getAllMovies();

	List<Genre> getAllGenres();

	List<Movie> getMoviesByGenre(int selectedGenre);

	Node getMainNode();

}
