package my.edu.taylors.oopj.movieticketing.view;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import my.edu.taylors.oopj.movieticketing.controller.MovieViewController;
import my.edu.taylors.oopj.movieticketing.entity.ShowTime;
import my.edu.taylors.oopj.movieticketing.view.listener.ShowTimeListener;

public class ShowTimeNode extends MyNode {
	
	private ShowTimeListener showTimeListener;

	private DatePicker picker;
	private VBox showTimeBox;

	public ShowTimeNode(ShowTimeListener showTimeListener) {
		this.showTimeListener = showTimeListener;

		showTimeBox = new VBox();
		showTimeBox.setStyle("-fx-border-style: none;");
		showTimeBox.getStyleClass().add("main-bg");
		getShowTimesByDay(Calendar.getInstance());
		mainNode = new VBox(getMenuBox(), showTimeBox);
	}

	private Node getMenuBox() {

		Label lbHeading = new Label("Show Times");
		lbHeading.getStyleClass().add("heading");

		final Calendar cal0 = Calendar.getInstance();
		Button btToday = new Button("Today");
		btToday.setCursor(Cursor.HAND);
		btToday.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(btToday, Priority.SOMETIMES);
		btToday.setOnAction(e -> updatePicker(cal0));
		btToday.getStyleClass().add("button");

		final Calendar cal1 = Calendar.getInstance();
		cal1.add(Calendar.DAY_OF_WEEK, 1);
		Button btTmrw = new Button("Tomorrow");
		btTmrw.setCursor(Cursor.HAND);
		btTmrw.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(btTmrw, Priority.SOMETIMES);
		btTmrw.setOnAction(e -> updatePicker(cal1));
		btTmrw.getStyleClass().add("button");

		final Calendar cal2 = Calendar.getInstance();
		cal2.add(Calendar.DAY_OF_WEEK, 2);
		Button bt2Days = new Button(getDayOfWeek(cal2.get(Calendar.DAY_OF_WEEK)));
		bt2Days.setCursor(Cursor.HAND);
		bt2Days.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(bt2Days, Priority.SOMETIMES);
		bt2Days.setOnAction(e -> updatePicker(cal2));
		bt2Days.getStyleClass().add("button");

		final Calendar cal3 = Calendar.getInstance();
		cal3.add(Calendar.DAY_OF_WEEK, 3);
		Button bt3Days = new Button(getDayOfWeek(cal3.get(Calendar.DAY_OF_WEEK)));
		bt3Days.setCursor(Cursor.HAND);
		bt3Days.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(bt3Days, Priority.SOMETIMES);
		bt3Days.setOnAction(e -> updatePicker(cal3));
		bt3Days.getStyleClass().add("button");

		final Calendar cal4 = Calendar.getInstance();
		cal4.add(Calendar.DAY_OF_WEEK, 4);
		Button bt4Days = new Button(getDayOfWeek(cal4.get(Calendar.DAY_OF_WEEK)));
		bt4Days.setCursor(Cursor.HAND);
		bt4Days.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(bt4Days, Priority.SOMETIMES);
		bt4Days.setOnAction(e -> updatePicker(cal4));
		bt4Days.getStyleClass().add("button");

		HBox daysBox = new HBox(btToday, btTmrw, bt2Days, bt3Days, bt4Days);
		daysBox.setSpacing(2);
		daysBox.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(daysBox, Priority.ALWAYS);

		initDatePicker();

		HBox topBox = new HBox(lbHeading, daysBox, picker);
		topBox.setSpacing(15);
		topBox.setPadding(new Insets(10));
		topBox.getStyleClass().add("main-bg");
		return topBox;
	}
	
	private void initDatePicker() {
		picker = new DatePicker();
		updatePicker(Calendar.getInstance());

		// allow only two month into the future and no past
		Callback<DatePicker, DateCell> customDayCellFactory = dp -> new DateCell() {
			@Override
			public void updateItem(LocalDate item, boolean empty) {
				super.updateItem(item, empty);
				if (item.isBefore(LocalDate.now()) || item.isAfter(LocalDate.now().plusMonths(2))) {
					setStyle("-fx-background-color: #ffcccc;");
					Platform.runLater(() -> setDisable(true));
				}
			}
		};
		picker.setDayCellFactory(customDayCellFactory);

		picker.setOnAction(e -> update());
	}

	public void update() {
		LocalDate localDate = picker.getValue();

		Date date = java.sql.Date.valueOf(localDate);
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);

		getShowTimesByDay(cal);
	}

	private void getShowTimesByDay(Calendar cal) {
		List<ShowTime> showTimes = showTimeListener.getShowTimesByDay(cal);
		renderShowTimes(showTimes);
	}

	private void renderShowTimes(List<ShowTime> showTimes) {
		showTimeBox.getChildren().clear();

		GridPane grid = new GridPane();
		ColumnConstraints column1 = new ColumnConstraints();
		column1.setPercentWidth(50);
		grid.getColumnConstraints().add(column1);

		ColumnConstraints column2 = new ColumnConstraints();
		column2.setPercentWidth(25);
		grid.getColumnConstraints().add(column2);

		ColumnConstraints column3 = new ColumnConstraints();
		column3.setPercentWidth(25);
		grid.getColumnConstraints().add(column3);

		grid.setVgap(20);
		grid.setPadding(new Insets(10));
		grid.getStyleClass().add("main-bg");

		for (int i = 0; i < showTimes.size(); i++) {
			ShowTime st = showTimes.get(i);

			Hyperlink hlMovieTitle = new Hyperlink(st.getMovieID().getMovieName());
			hlMovieTitle.setOnAction(e -> showMovieDetail(st.getMovieID().getId()));
			hlMovieTitle.setId("h3");
			StackPane paneTitle = new StackPane(hlMovieTitle);
			paneTitle.getStyleClass().add("grid-bg");
			grid.add(paneTitle, 0, i);

			Calendar cal = Calendar.getInstance();
			cal.setTime(st.getTime());
			int hour = cal.get(Calendar.HOUR);
			int minuteInt = cal.get(Calendar.MINUTE);
			String minute = (minuteInt < 10 ? "0" : "") + minuteInt;
			String AM_PM = cal.get(Calendar.AM_PM) == Calendar.AM ? "am" : "pm";
			if (hour == 0) hour = 12;
			String time = hour + "." + minute + " " + AM_PM;

			Label lbTime = new Label(time);
			lbTime.setId("h3");
			StackPane paneTime = new StackPane(lbTime);
			paneTime.getStyleClass().add("grid-bg");
			grid.add(paneTime, 1, i);
			
			Button btBuy = new Button("Buy Ticket");
			btBuy.getStyleClass().add("button-white");
			StackPane paneBuy = new StackPane(btBuy);
			paneBuy.getStyleClass().add("grid-bg");

			grid.add(paneBuy, 2, i);
			btBuy.setOnAction(e -> goBuyTicket(st.getId()));
		}

		ScrollPane scroll = new ScrollPane(grid);
		scroll.setStyle("-fx-border-style: none;");
		scroll.getStyleClass().add("main-bg");
		scroll.setHbarPolicy(ScrollBarPolicy.NEVER);
		scroll.setFitToWidth(true);

		showTimeBox.getChildren().add(scroll);
	}

	private void goBuyTicket(int id) {
		showTimeListener.goBuyTicket(id);
	}

	private void showMovieDetail(int id) {
		new MovieViewController(id);
	}

	private void updatePicker(Calendar cal) {
		LocalDate localDate = new java.sql.Date(cal.getTime().getTime()).toLocalDate();
		picker.setValue(localDate);
	}

	private String getDayOfWeek(int dayNumber) {
		switch (dayNumber) {
			case Calendar.MONDAY: return "Monday";
			case Calendar.TUESDAY: return "Tuesday";
			case Calendar.WEDNESDAY: return "Wednesday";
			case Calendar.THURSDAY: return "Thursday";
			case Calendar.FRIDAY: return "Friday";
			case Calendar.SATURDAY: return "Saturday";
			default: return "Sunday";
		}
	}

}
