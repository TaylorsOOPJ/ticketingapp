package my.edu.taylors.oopj.movieticketing.dao;

import java.util.List;

import my.edu.taylors.oopj.movieticketing.entity.Genre;

public interface GenreDao extends IAbstractDao<Genre> {

	List<Genre> getMovieGenres(int movieId);

}
