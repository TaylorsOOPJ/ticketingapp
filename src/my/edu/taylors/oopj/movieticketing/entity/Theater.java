package my.edu.taylors.oopj.movieticketing.entity;

import my.edu.taylors.oopj.movieticketing.db.AbstractEntity;

public class Theater extends AbstractEntity {

	private String theaterName;
	private String location;

	public Theater(String theaterName, String location) {
		super();
		this.theaterName = theaterName;
		this.location = location;
	}

	public Theater(int id, String theaterName, String location) {
		super(id);
		this.theaterName = theaterName;
		this.location = location;
	}

	public String getTheaterName() {
		return theaterName;
	}

	public void setTheaterName(String theaterName) {
		this.theaterName = theaterName;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	@Override
	public String toString() {
		return "Theater [theaterName=" + theaterName + ", location=" + location + ", id=" + id + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((location == null) ? 0 : location.hashCode());
		result = prime * result + ((theaterName == null) ? 0 : theaterName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Theater other = (Theater) obj;
		if (location == null) {
			if (other.location != null)
				return false;
		} else if (!location.equals(other.location))
			return false;
		if (theaterName == null) {
			if (other.theaterName != null)
				return false;
		} else if (!theaterName.equals(other.theaterName))
			return false;
		return true;
	}

}
