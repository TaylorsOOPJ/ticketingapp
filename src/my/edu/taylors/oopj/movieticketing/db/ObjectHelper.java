package my.edu.taylors.oopj.movieticketing.db;

import java.lang.reflect.Field;
import java.sql.Timestamp;
import my.edu.taylors.oopj.movieticketing.db.annotation.ManyToOne;
import my.edu.taylors.oopj.movieticketing.db.annotation.NotNull;

class ObjectHelper {
	
	/**
	 * Takes an entity and goes through all its fields<br>
	 * Fills SqlValues object with names and values of found fields<br>
	 * Takes into consideration {@link ManyToOne} and {@link NotNull} annotations
	 * @param entity
	 * @return SqlValues
	 */
	static SqlValues readEntityFieldsAndValues(Object entity) {

		SqlValues vals = new SqlValues();

		try {
			Field[] fields = entity.getClass().getDeclaredFields();
			for (int i = 0; i < fields.length; i++) {

				Field f = fields[i];
				f.setAccessible(true);

				// Integer
				if (f.getType().isAssignableFrom(Integer.TYPE)) {

					Integer value = (Integer) f.get(entity);
					vals.getIntVals().put(f.getName(), value);

				// Float
				} else if (f.getType().isAssignableFrom(Float.TYPE)) {

					Float value = (Float) f.get(entity);
					vals.getFloatVals().put(f.getName(), value);

				// Double
				} else if (f.getType().isAssignableFrom(Double.TYPE)) {

					Double value = (Double) f.get(entity);
					vals.getDoubleVals().put(f.getName(), value);

				// Boolean
				} else if (f.getType().isAssignableFrom(Boolean.TYPE)) {

					Boolean value = (Boolean) f.get(entity);
					int intVal = value ? 1 : 0;
					vals.getIntVals().put(f.getName(), intVal);

				// String
				} else if (f.getType().isAssignableFrom(String.class)) {

					String value = (String) f.get(entity);
					vals.getStringVals().put(f.getName(), value);

				// Datetime
				} else if (f.getType().isAssignableFrom(java.util.Date.class)) {
					
					java.util.Date value = (java.util.Date) f.get(entity);
					Timestamp stamp = new Timestamp(value.getTime());
					vals.getDateVals().put(f.getName(), stamp);
				
				// Enum
				} else if (f.getType().isEnum()) {

					String value = ((Enum<?>) f.get(entity)).name();
					vals.getStringVals().put(f.getName(), value);

				// Custom class marked with @ManyToOne
				} else if (f.getDeclaredAnnotation(ManyToOne.class) != null) {

					AbstractEntity value = (AbstractEntity) f.get(entity);
					if (value == null) {
						// if cannot be null
						if (f.getDeclaredAnnotation(NotNull.class) != null) {
							throw new NullPointerException(
									"Field " + f.getName() + " in " + entity.getClass().getName()
									+ " is marked as @NotNull, but its value is null.");
						} else {
							vals.getStringVals().put(f.getName(), null);
						}
					} else {
						vals.getIntVals().put(f.getName(), value.getId());
					}

				}
			}
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return vals;
	}

}
