package my.edu.taylors.oopj.movieticketing.controller;

import java.util.List;

import my.edu.taylors.oopj.movieticketing.Router;
import my.edu.taylors.oopj.movieticketing.entity.Customer;
import my.edu.taylors.oopj.movieticketing.entity.Theater;
import my.edu.taylors.oopj.movieticketing.service.impl.HomeServiceImpl;
import my.edu.taylors.oopj.movieticketing.view.HomeView;
import my.edu.taylors.oopj.movieticketing.view.listener.HomeListener;

public class HomeController extends AbstractController implements HomeListener {

	private final HomeServiceImpl homeService;

	public HomeController(Customer customer) {
		homeService = new HomeServiceImpl();
		view = new HomeView(this, customer);
	}

	@Override
	public void logout() {
		Router.getInstance().navigate(new LoginController());
	}

	@Override
	public List<Theater> getAllTheters() {
		return homeService.getAllTheaters();
	}

}
