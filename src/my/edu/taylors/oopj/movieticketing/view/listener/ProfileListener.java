package my.edu.taylors.oopj.movieticketing.view.listener;

import java.util.List;

import javafx.scene.Node;
import my.edu.taylors.oopj.movieticketing.entity.Customer;
import my.edu.taylors.oopj.movieticketing.entity.Payment;
import my.edu.taylors.oopj.movieticketing.view.ProfileNode;
import my.edu.taylors.oopj.movieticketing.view.dto.UpdateCustomerDto;

public interface ProfileListener {

	Customer getLoggedCustomer();

	List<Payment> getTicketsByCustomer(Customer customer);

	Node getMainNode();

	void updateCustomer(UpdateCustomerDto updateCustomerDto);

	void updatePassword(String newPassword);

	ProfileNode getProfileNode();

}
