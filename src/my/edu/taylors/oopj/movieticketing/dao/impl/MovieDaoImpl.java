package my.edu.taylors.oopj.movieticketing.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import my.edu.taylors.oopj.movieticketing.dao.MovieDao;
import my.edu.taylors.oopj.movieticketing.db.DatabaseConnection;
import my.edu.taylors.oopj.movieticketing.entity.Accessibility;
import my.edu.taylors.oopj.movieticketing.entity.Movie;

public class MovieDaoImpl extends AbstractDao<Movie> implements MovieDao {

	public static final String SQL =
			"m.id, m.movieName, m.movieRating, m.description, m.accessibility, m.releaseDate, m.imdb, m.img, m.isHot, m.trailer";

	public static Movie getMovieFromResult(ResultSet results) throws SQLException {

		int id = results.getInt("m.id");
		String movieName = results.getString("m.movieName");
		int movieRating = results.getInt("m.movieRating");
		String description = results.getString("m.description");
		Accessibility accessibility = Accessibility.valueOf(results.getString("m.accessibility"));
		Timestamp releaseDateStamp = results.getTimestamp("m.releaseDate");
		Date releaseDate = new Date(releaseDateStamp.getTime());
		String imdb = results.getString("m.imdb");
		String img = results.getString("m.img");
		boolean isHot = results.getBoolean("m.isHot");
		String trailer = results.getString("m.trailer");

		return new Movie(id, movieName, movieRating, description, accessibility, releaseDate, imdb, img, isHot, trailer);
	}

	@Override
	public Movie getByKey(int key) {
		List<Movie> movies = find("WHERE m.id = ?", key);
		return movies.size() > 0 ? movies.get(0) : null;
	}

	@Override
	public List<Movie> getByName(String name) {
		return find("WHERE m.movieName = ?", name);
	}

	@Override
	public List<Movie> findAll() {
		return find("");
	}

	@Override
	public List<Movie> findAllByGenreId(int selectedGenre) {
		return find("WHERE mg.genreID = ?", selectedGenre);
	}

	@Override
	public List<Movie> findAllByHot() {
		return find("WHERE m.isHot = 1");
	}

	private List<Movie> find(String where, Object... args) {

		try {
			Connection conn = DatabaseConnection.getInstance().getConnection();

			String sql = 
				"SELECT " + MovieDaoImpl.SQL + " " +
				"FROM movie m " +
				"LEFT OUTER JOIN movie_genre mg ON mg.movieID = m.id " + where + " " +
				"GROUP BY m.id";
			PreparedStatement selectStatement = conn.prepareStatement(sql);
			for (int i = 0; i < args.length; i++) {
				selectStatement.setObject(i+1, args[i]);
			}

			ResultSet results = selectStatement.executeQuery();
			List<Movie> movies = new ArrayList<>();

			while (results.next()) {
				Movie movie = getMovieFromResult(results);
				movies.add(movie);
			}

			return movies;
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return new ArrayList<>(0);
	}


}
