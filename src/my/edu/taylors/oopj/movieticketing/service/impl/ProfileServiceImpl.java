package my.edu.taylors.oopj.movieticketing.service.impl;

import java.util.List;

import org.mindrot.jbcrypt.BCrypt;

import my.edu.taylors.oopj.movieticketing.dao.CustomerDao;
import my.edu.taylors.oopj.movieticketing.dao.PaymentDao;
import my.edu.taylors.oopj.movieticketing.dao.impl.CustomerDaoImpl;
import my.edu.taylors.oopj.movieticketing.dao.impl.PaymentDaoImpl;
import my.edu.taylors.oopj.movieticketing.entity.Customer;
import my.edu.taylors.oopj.movieticketing.entity.Payment;
import my.edu.taylors.oopj.movieticketing.service.ProfileService;

public class ProfileServiceImpl implements ProfileService {

	private PaymentDao paymentDao;
	private CustomerDao customerDao;

	public ProfileServiceImpl() {
		paymentDao = new PaymentDaoImpl();
		customerDao = new CustomerDaoImpl();
	}

	@Override
	public List<Payment> getTicketsByCustomer(Customer customer) {
		return paymentDao.getPaymentsByCustomer(customer);
	}

	@Override
	public void updateCustomer(Customer customer) {
		customerDao.update(customer);
	}

	@Override
	public void updatePassword(Customer customer, String newPassword) {
		String hashed = BCrypt.hashpw(newPassword, BCrypt.gensalt(11));
		customer.setPassword(hashed);
		customerDao.update(customer);
	}

}
