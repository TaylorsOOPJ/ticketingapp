package my.edu.taylors.oopj.movieticketing.utils;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;

import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.effect.PerspectiveTransform;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.util.Duration;
import my.edu.taylors.oopj.movieticketing.controller.MovieViewController;
import my.edu.taylors.oopj.movieticketing.entity.Movie;

/**
 * 
 * @author ggrec
 */
public class FlipAnimation {

// ==================== Static Fields ========================

    private static final Double PIE = Math.PI;
    private static final Double HALF_PIE = Math.PI / 2;
    private static final double ANIMATION_DURATION = 2000;
    private static final double ANIMATION_RATE = 10;
    private static final double WIDTH = 510/3;
    private static final double HEIGHT = 755/3;


// ====================== Instance Fields =============================

    private Timeline animation;

    private StackPane flipPane;

    private SimpleDoubleProperty angle = new SimpleDoubleProperty(HALF_PIE);

    private final PerspectiveTransform transform = new PerspectiveTransform();

    private final SimpleBooleanProperty flippedProperty = new SimpleBooleanProperty(true);


// ==================== Creators ====================


    public StackPane createFlipPane(Movie movie) {
        angle = createAngleProperty();

        flipPane = new StackPane();
        flipPane.setPadding(new Insets(30));

        flipPane.getChildren().addAll(createBackNode(movie), createFrontNode(movie.getImg()));

        flipPane.widthProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            recalculateTransformation(angle.doubleValue());
        });

        flipPane.heightProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            recalculateTransformation(angle.doubleValue());
        });

        return flipPane;
    }

    private StackPane createFrontNode(String img) {
        final StackPane node = new StackPane();

        ImageView ivPoster = new ImageView(new Image(img));
        ivPoster.setFitWidth(WIDTH);
        ivPoster.setFitHeight(HEIGHT);

        node.setEffect(transform);
        node.visibleProperty().bind(flippedProperty);

        node.getChildren().addAll(createMovieImage(ivPoster)); 

        node.setOnMouseEntered(e -> flip());
        return node;
    }

    private StackPane createBackNode(Movie movie) {
        final StackPane node = new StackPane();

        node.setEffect(transform);
        node.visibleProperty().bind(flippedProperty.not());
 
        Hyperlink url = new Hyperlink("IMDB");
        url.setId("url");
        url.setOnAction(e -> {
            try {
                Desktop.getDesktop().browse(new URI(movie.getImdb()));
            } catch (IOException | URISyntaxException ex) {
                ex.printStackTrace();
                AlertFactory.createAlert(Alert.AlertType.ERROR, "Error", "Link cannot be open.");
            }
        });

        Hyperlink hlMovieTitle = new Hyperlink(movie.getMovieName());
        hlMovieTitle.setOnAction(e -> showMovieDetail(movie.getId()));
        hlMovieTitle.setId("h3");

        Label lbRating = new Label("Rating: " + movie.getMovieRating());
        lbRating.setId("h3");

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yy");
        String date = dateFormat.format(movie.getReleaseDate());
        Label lbReleaseDate = new Label("Release Date: " + date);
        lbReleaseDate.setId("h3");

        VBox info = new VBox();
        info.setAlignment(Pos.CENTER);
        info.getChildren().addAll(hlMovieTitle, lbRating, lbReleaseDate, url);

        node.getChildren().addAll(info); 
        node.setOnMouseExited(e -> flip());
        node.setPrefWidth(WIDTH+50);
        return node;
    }

    private void showMovieDetail(int id) {
        new MovieViewController(id);
    }

    private Label createMovieImage(final Node img) {
        final Label label = new Label();
        label.setGraphic(img);
        label.setMaxHeight(Double.MAX_VALUE);
        label.setMaxWidth(Double.MAX_VALUE);

        return label;
    }

    private SimpleDoubleProperty createAngleProperty() {

        final SimpleDoubleProperty angle = new SimpleDoubleProperty(HALF_PIE);

        angle.addListener((final ObservableValue<? extends Number> obsValue, final Number oldValue, final Number newValue) -> {
            recalculateTransformation(newValue.doubleValue());
        });

        return angle;
    }

    private Timeline createAnimation() {
        return new Timeline(

                new KeyFrame(Duration.millis(0),    new KeyValue(angle, HALF_PIE)),

                new KeyFrame(Duration.millis(ANIMATION_DURATION / 2),  new KeyValue(angle, 0, Interpolator.EASE_IN)),

                new KeyFrame(Duration.millis(ANIMATION_DURATION / 2), (final ActionEvent arg0) -> {
                    flippedProperty.set( flippedProperty.not().get() );
                }),

                new KeyFrame(Duration.millis(ANIMATION_DURATION / 2),  new KeyValue(angle, PIE)),

                new KeyFrame(Duration.millis(ANIMATION_DURATION), new KeyValue(angle, HALF_PIE, Interpolator.EASE_OUT))

        );
    }

// ==================== Action Methods ====================

    private void flip() {
        if (animation == null) {
            animation = createAnimation();
        }

        animation.setRate( flippedProperty.get() ? ANIMATION_RATE : -ANIMATION_RATE );
        animation.play();
    }

// ==================== Business Methods ====================

    private void recalculateTransformation(final double angle) {
        final double insetsTop = flipPane.getInsets().getTop() * 2;
        final double insetsLeft = flipPane.getInsets().getLeft() * 2;

        final double radius = flipPane.widthProperty().subtract(insetsLeft).divide(2).doubleValue();
        final double height = flipPane.heightProperty().subtract(insetsTop).doubleValue();
        final double back = height / 10;

        transform.setUlx(radius - Math.sin(angle) * radius);
        transform.setUly(0 - Math.cos(angle) * back);
        transform.setUrx(radius + Math.sin(angle) * radius);
        transform.setUry(0 + Math.cos(angle) * back);
        transform.setLrx(radius + Math.sin(angle) * radius);
        transform.setLry(height - Math.cos(angle) * back);
        transform.setLlx(radius - Math.sin(angle) * radius);
        transform.setLly(height + Math.cos(angle) * back);
    }

}
