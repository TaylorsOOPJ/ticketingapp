package my.edu.taylors.oopj.movieticketing.db;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

/**
 * Class for holding names and values of entity fields
 */
class SqlValues {

	private Map<String, String> stringVals;
	private Map<String, Integer> intVals;
	private Map<String, Float> floatVals;
	private Map<String, Double> doubleVals;
	private Map<String, Timestamp> dateVals;
	
	SqlValues() {
		stringVals = new HashMap<>();
		intVals = new HashMap<>();
		floatVals = new HashMap<>();
		doubleVals = new HashMap<>();
		dateVals = new HashMap<>();
	}

	public Map<String, String> getStringVals() {
		return stringVals;
	}

	public Map<String, Integer> getIntVals() {
		return intVals;
	}

	public Map<String, Timestamp> getDateVals() {
		return dateVals;
	}

	public Map<String, Float> getFloatVals() {
		return floatVals;
	}

	public Map<String, Double> getDoubleVals() {
		return doubleVals;
	}

}
