package my.edu.taylors.oopj.movieticketing.service;

import java.util.Calendar;
import java.util.List;

import my.edu.taylors.oopj.movieticketing.entity.ShowTime;
import my.edu.taylors.oopj.movieticketing.entity.Theater;

public interface ShowTimeService {

	List<ShowTime> getShowTimesByDay(int theaterId, Calendar cal);

	Theater getTheaterById(int theaterId);

}
