package my.edu.taylors.oopj.movieticketing.service.impl;

import org.mindrot.jbcrypt.BCrypt;

import my.edu.taylors.oopj.movieticketing.dao.CustomerDao;
import my.edu.taylors.oopj.movieticketing.dao.impl.CustomerDaoImpl;
import my.edu.taylors.oopj.movieticketing.entity.Customer;
import my.edu.taylors.oopj.movieticketing.service.LoginService;

public class LoginServiceImpl implements LoginService {

	private CustomerDao customerDao;

	public LoginServiceImpl() {
		customerDao = new CustomerDaoImpl();
	}

	@Override
	public boolean isCustomerEmailDuplicate(Customer customer) {
		Customer customer2 = customerDao.getByEmail(customer.getEmail());
		if (customer2 == null) {
			return false;
		} else if (customer2.getId() == customer.getId()) {
			return false;
		} else {
			return true;
		}
	}

	@Override
	public boolean authenticate(String email, String password) {
		Customer customer = customerDao.getByEmail(email);
		if (customer == null) return false;
		return BCrypt.checkpw(password, customer.getPassword());
	}

	@Override
	public void insertCustomer(Customer entity) {
		String hashed = BCrypt.hashpw(entity.getPassword(), BCrypt.gensalt(11));
		entity.setPassword(hashed);
		customerDao.insert(entity);
	}

	@Override
	public Customer getCustomerByEmail(String email) {
		return customerDao.getByEmail(email);
	}

}
