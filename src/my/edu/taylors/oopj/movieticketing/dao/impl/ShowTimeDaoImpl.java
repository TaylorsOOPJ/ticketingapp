package my.edu.taylors.oopj.movieticketing.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import my.edu.taylors.oopj.movieticketing.dao.ShowTimeDao;
import my.edu.taylors.oopj.movieticketing.db.DatabaseConnection;
import my.edu.taylors.oopj.movieticketing.entity.Movie;
import my.edu.taylors.oopj.movieticketing.entity.ShowTime;
import my.edu.taylors.oopj.movieticketing.entity.Theater;

public class ShowTimeDaoImpl extends AbstractDao<ShowTime> implements ShowTimeDao {

	public static final String SQL = "st.id, st.time, st.price, st.ticketsAvailable";

	public static ShowTime getShowTimeFromResult(ResultSet results, Movie movie, Theater theater) throws SQLException {
		int id = results.getInt("st.id");
		Timestamp timeStamp = results.getTimestamp("st.time");
		Date time = new Date(timeStamp.getTime());
		float price = results.getFloat("st.price");
		int ticketsAvailable = results.getInt("st.ticketsAvailable");

		return new ShowTime(id, time, price, ticketsAvailable, movie, theater);
	}

	@Override
	public ShowTime getByKey(int key) {
		List<ShowTime> showTimes = find("WHERE st.id = ?", key);
		return showTimes.size() > 0 ? showTimes.get(0) : null;
	}

	@Override
	public List<ShowTime> findAll() {
		return find("");
	}

	@Override
	public List<ShowTime> findByDay(int theaterId, Calendar cal) {
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH) + 1;
		int day = cal.get(Calendar.DAY_OF_MONTH);
		String date = year + "-" + month + "-" + day;
		String date1 = date + " 00:00:00";
		String date2 = date + " 23:59:59";
		return find("WHERE st.theaterID = ? AND st.time between ? and ?", theaterId, date1, date2);
		//where theaterID = 1 AND time between '2016-11-17 00:00:00' AND '2016-11-17 23:59:59';
	}

	@Override
	public List<ShowTime> findByMovieAndTheatre(int movieId, int theaterId) {
		return find("WHERE m.id = ? AND t.id = ? AND st.time > NOW()", movieId, theaterId);
	}

	private List<ShowTime> find(String where, Object... args) {

		try {
			Connection conn = DatabaseConnection.getInstance().getConnection();

			String sql = 
				"SELECT " + ShowTimeDaoImpl.SQL + " , " + MovieDaoImpl.SQL + ", " + TheaterDaoImpl.SQL + " " +
				"FROM showtime st " +
				"INNER JOIN movie m ON st.movieID = m.id " +
				"INNER JOIN theater t ON st.theaterID = t.id " + where;
			PreparedStatement selectStatement = conn.prepareStatement(sql);
			for (int i = 0; i < args.length; i++) {
				selectStatement.setObject(i+1, args[i]);
			}

			ResultSet results = selectStatement.executeQuery();
			List<ShowTime> showTimes = new ArrayList<>();

			while (results.next()) {

				Theater theater = TheaterDaoImpl.getTheaterFromResult(results);
				Movie movie = MovieDaoImpl.getMovieFromResult(results);
				ShowTime showTime = getShowTimeFromResult(results, movie, theater);

				showTimes.add(showTime);
			}

			return showTimes;
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return new ArrayList<>(0);
	}

}
