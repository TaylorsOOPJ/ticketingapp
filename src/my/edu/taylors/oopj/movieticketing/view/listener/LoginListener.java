package my.edu.taylors.oopj.movieticketing.view.listener;

import my.edu.taylors.oopj.movieticketing.view.dto.CreateCustomerDto;

public interface LoginListener {

	void customerCreated(CreateCustomerDto event);

	void login(String email, String password);

}
