package my.edu.taylors.oopj.movieticketing.view;

import java.util.List;

import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import my.edu.taylors.oopj.movieticketing.controller.MovieViewController;
import my.edu.taylors.oopj.movieticketing.entity.Genre;
import my.edu.taylors.oopj.movieticketing.entity.Movie;
import my.edu.taylors.oopj.movieticketing.view.listener.GenresListener;

public class GenresNode extends MyNode {

	private GenresListener genresListener;
	private int selectedGenre;
	private List<Genre> genres;
	private HBox genresBox;

	public GenresNode(GenresListener genresListener) {
		this.genresListener = genresListener;
		selectedGenre = 0;
		genres = genresListener.getAllGenres();
		mainNode = new VBox();
		mainNode.setStyle("-fx-background-color: #263238;");
		renderButtons();
		renderMovies();
	}

	private void renderButtons() {

		genresBox = new HBox();

		Button btnAll = new Button("All");
		btnAll.setOnAction(e -> changeGenre(0));
		btnAll.setId("genre0");
		btnAll.getStyleClass().add("button");
		btnAll.getStyleClass().add("button-white");
		btnAll.setCursor(Cursor.HAND);
		btnAll.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(btnAll, Priority.SOMETIMES);
		genresBox.getChildren().add(btnAll);

		for (int i = 0; i < genres.size(); i++) {
			Button btn = new Button(genres.get(i).getName());
			final int id = genres.get(i).getId();
			btn.setOnAction(e -> changeGenre(id));
			btn.setId("genre"+id);
			btn.getStyleClass().add("button");
			btn.setCursor(Cursor.HAND);
			btn.setMaxWidth(Double.MAX_VALUE);
			HBox.setHgrow(btn, Priority.SOMETIMES);
			genresBox.getChildren().add(btn);
		}

	}

	public void renderMovies() {

		List<Movie> movies;
		if (selectedGenre == 0) {
			movies = genresListener.getAllMovies();
		} else {
			movies = genresListener.getMoviesByGenre(selectedGenre);
		}

		Pane moviesBox;
		if (movies.size() == 0) {
			moviesBox = new StackPane();
			final Label lbNotFound = new Label("No movies found.");
			lbNotFound.setFont(Font.font("Arial", 25));
			lbNotFound.setMaxWidth(Double.MAX_VALUE);
			lbNotFound.setAlignment(Pos.CENTER);
			moviesBox.getChildren().add(lbNotFound);
		} else {

			moviesBox = new FlowPane(Orientation.HORIZONTAL);
			moviesBox.setPadding(new Insets(10));

			for (int i = 0; i < movies.size(); i++) {
				Movie movie = movies.get(i);
				ImageView ivPoster = new ImageView(new Image(movie.getImg()));
				ivPoster.setFitWidth(510/3);
				ivPoster.setFitHeight(755/3);

				Hyperlink hlMovieTitle = new Hyperlink(movie.getMovieName());
				hlMovieTitle.setStyle("-fx-text-fill: #FFFFFF;\n" +
					 "-fx-font-family: Raleway;\n" +
					 "-fx-font-size: 20px;\n" +
					 "-fx-font-weight: 400;");
				hlMovieTitle.setWrapText(true);
				hlMovieTitle.setOnAction(e -> showMovieDetail(movie.getId()));
				hlMovieTitle.setFont(Font.font(22));

				Label lbMovieDesc = new Label(movie.getDescription());
				lbMovieDesc.setStyle("-fx-text-fill: #FFFFFF;\n" +
					 "-fx-font-family: Raleway;\n" +
					 "-fx-font-size: 12px;\n" +
					 "-fx-font-weight: 300;");
				lbMovieDesc.setWrapText(true);

				VBox movieInfo = new VBox(hlMovieTitle, lbMovieDesc);
				movieInfo.setPadding(new Insets(0, 0, 0, 10));
				movieInfo.setSpacing(10);
				movieInfo.setMaxWidth(200);

				HBox movieBox = new HBox(ivPoster, movieInfo);
				movieBox.setPadding(new Insets(0, 10, 10, 0));
				moviesBox.getChildren().add(movieBox);
			}
		}

		ScrollPane scrollPane = new ScrollPane(moviesBox);
		scrollPane.setFitToWidth(true);
		scrollPane.setFitToHeight(true);
		moviesBox.setStyle("-fx-background-color: #37474F");
		((VBox)mainNode).getChildren().clear();
		((VBox)mainNode).getChildren().add(genresBox);
		((VBox)mainNode).getChildren().add(scrollPane);
	}

	private void showMovieDetail(int id) {
		new MovieViewController(id);
	}

	private void changeGenre(int selectedGenre) {
		mainNode.lookup("#genre" + this.selectedGenre).getStyleClass().remove("button-white");
		this.selectedGenre = selectedGenre;
		mainNode.lookup("#genre" + selectedGenre).getStyleClass().add("button-white");

		renderMovies();
	}

}
