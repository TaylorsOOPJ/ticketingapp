package my.edu.taylors.oopj.movieticketing.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.Map;

/**
 * This class reads entity fields, calls SqlBuilder a creates necessary SQL statements which then returns
 */
class StatementBuilder {

	private Connection connection;
	private SqlBuilder sqlBuilder;

	StatementBuilder() {
		connection = DatabaseConnection.getInstance().getConnection();
		sqlBuilder = new SqlBuilder();
	}

	/**
	 * Get simple class name which is used as table name
	 * @param entity
	 * @return lower cased class name
	 */
	String getTableName(Object entity) {
		return entity.getClass().getSimpleName().toLowerCase();
	}
	
	/**
	 * Get full class name, including packages
	 * @param entity
	 * @return full class name
	 */
	String getFullClassName(Object entity) {
		return entity.getClass().getName();
	}

	/**
	 * Prepare insert statement
	 * @param entity
	 * @return INSERT PreparedStatement
	 * @throws SQLException
	 */
	PreparedStatement getInsertStmt(AbstractEntity entity) throws SQLException {

		SqlValues vals = ObjectHelper.readEntityFieldsAndValues(entity);
		String tableName = getTableName(entity);
		String sql = sqlBuilder.getInsertSQL(tableName, vals);

		PreparedStatement stmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
		bindParams(stmt, vals);

		return stmt;
	}

	/**
	 * Prepare update statement
	 * @param entity
	 * @return UPDATE PreparedStatement
	 * @throws SQLException
	 */
	PreparedStatement getUpdateStmt(AbstractEntity entity) throws SQLException {

		SqlValues vals = ObjectHelper.readEntityFieldsAndValues(entity);
		String tableName = getTableName(entity);
		String sql = sqlBuilder.getUpdateSQL(tableName, vals);

		PreparedStatement stmt = connection.prepareStatement(sql);
		int j = bindParams(stmt, vals);
		stmt.setInt(j++, entity.getId());

		return stmt;
	}

	/**
	 * Prepare delete statement
	 * @param aEntity
	 * @return DELETE PreparedStatement
	 * @throws SQLException
	 */
	PreparedStatement getDeleteStmt(AbstractEntity aEntity) throws SQLException {
		String sql = "DELETE FROM " + getTableName(aEntity) + " WHERE id = ?";
		System.out.println(sql);
		PreparedStatement stmt = connection.prepareStatement(sql);
		stmt.setInt(1, aEntity.getId());
		
		return stmt;
	}

	/**
	 * Goes through all previously read SqlValues and binds values into the PreparedStatement
	 * @param stmt PreparedStatement
	 * @param sqlValues SqlValues
	 * @return next sequence number for binding
	 * @throws SQLException
	 */
	private int bindParams(PreparedStatement stmt, SqlValues sqlValues) throws SQLException {

		Map<String, String> stringMap = sqlValues.getStringVals();
		int j = 1;
		for (Map.Entry<String, String> entry : stringMap.entrySet()) {
			if (entry.getValue() == null) {
				stmt.setNull(j++, Types.VARCHAR);
			} else {
				stmt.setString(j++, entry.getValue());
			}
		}

		Map<String, Integer> intMap = sqlValues.getIntVals();
		for (Map.Entry<String, Integer> entry : intMap.entrySet()) {
			if (entry.getValue() == null) {
				stmt.setNull(j++, Types.INTEGER);
			} else {
				stmt.setInt(j++, entry.getValue());
			}
		}

		Map<String, Float> floatMap = sqlValues.getFloatVals();
		for (Map.Entry<String, Float> entry : floatMap.entrySet()) {
			if (entry.getValue() == null) {
				stmt.setNull(j++, Types.FLOAT);
			} else {
				stmt.setFloat(j++, entry.getValue());
			}
		}

		Map<String, Double> doubleMap = sqlValues.getDoubleVals();
		for (Map.Entry<String, Double> entry : doubleMap.entrySet()) {
			if (entry.getValue() == null) {
				stmt.setNull(j++, Types.DOUBLE);
			} else {
				stmt.setDouble(j++, entry.getValue());
			}
		}

		Map<String, Timestamp> dateMap = sqlValues.getDateVals();
		for (Map.Entry<String, Timestamp> entry : dateMap.entrySet()) {
			if (entry.getValue() == null) {
				stmt.setNull(j++, Types.TIMESTAMP);
			} else {
				stmt.setTimestamp(j++, entry.getValue());
			}
		}

		return j;
	}

}
