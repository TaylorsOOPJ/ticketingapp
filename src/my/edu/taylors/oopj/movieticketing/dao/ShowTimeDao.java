package my.edu.taylors.oopj.movieticketing.dao;

import java.util.Calendar;
import java.util.List;

import my.edu.taylors.oopj.movieticketing.entity.ShowTime;

public interface ShowTimeDao extends IAbstractDao<ShowTime> {

	List<ShowTime> findByDay(int theaterId, Calendar cal);

	List<ShowTime> findByMovieAndTheatre(int movieId, int theaterId);

}
