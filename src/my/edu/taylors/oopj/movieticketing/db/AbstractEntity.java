package my.edu.taylors.oopj.movieticketing.db;

/**
 * Abstract entity for any entity in the project<br>
 * Forces entities to have one id named "id"<br>
 * Default value for id is 0, which means the entity is not persisted
 */
public abstract class AbstractEntity {

	protected int id;
	
	protected AbstractEntity() {
		this.id = 0;
	}

	protected AbstractEntity(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractEntity other = (AbstractEntity) obj;
		if (id != other.id)
			return false;
		return true;
	}

}
