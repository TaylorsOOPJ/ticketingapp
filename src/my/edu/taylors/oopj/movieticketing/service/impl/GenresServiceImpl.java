package my.edu.taylors.oopj.movieticketing.service.impl;

import java.util.List;

import my.edu.taylors.oopj.movieticketing.dao.GenreDao;
import my.edu.taylors.oopj.movieticketing.dao.MovieDao;
import my.edu.taylors.oopj.movieticketing.dao.impl.GenreDaoImpl;
import my.edu.taylors.oopj.movieticketing.dao.impl.MovieDaoImpl;
import my.edu.taylors.oopj.movieticketing.entity.Genre;
import my.edu.taylors.oopj.movieticketing.entity.Movie;
import my.edu.taylors.oopj.movieticketing.service.GenresService;

public class GenresServiceImpl implements GenresService {

	private MovieDao movieDao;
	private GenreDao genreDao;

	public GenresServiceImpl() {
		movieDao = new MovieDaoImpl();
		genreDao = new GenreDaoImpl();
	}

	@Override
	public List<Movie> getAllMovies() {
		return movieDao.findAll();
	}

	@Override
	public List<Genre> getAllGenres() {
		return genreDao.findAll();
	}

	@Override
	public List<Movie> getMoviesByGenre(int selectedGenre) {
		return movieDao.findAllByGenreId(selectedGenre);
	}

}
