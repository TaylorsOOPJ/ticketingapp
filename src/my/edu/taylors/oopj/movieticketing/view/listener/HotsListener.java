package my.edu.taylors.oopj.movieticketing.view.listener;

import java.util.List;
import javafx.scene.Node;
import my.edu.taylors.oopj.movieticketing.entity.Movie;

/**
 *
 * @author Victor
 */
public interface HotsListener {

	public List <Movie> getHotMovies();

	Node getMainNode();

}