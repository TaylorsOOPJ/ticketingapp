package my.edu.taylors.oopj.movieticketing.controller;

import my.edu.taylors.oopj.movieticketing.entity.Payment;
import my.edu.taylors.oopj.movieticketing.entity.ShowTime;
import my.edu.taylors.oopj.movieticketing.service.TicketBookingService;
import my.edu.taylors.oopj.movieticketing.service.impl.TicketBookingServiceImpl;
import my.edu.taylors.oopj.movieticketing.view.ProfileNode;
import my.edu.taylors.oopj.movieticketing.view.TicketBookingView;
import my.edu.taylors.oopj.movieticketing.view.listener.TicketBookingListener;

public class TicketBookingController extends AbstractController implements TicketBookingListener {

	private TicketBookingService ticketBookingService;
	private ProfileNode profileNode;

	public TicketBookingController(int showTimeId, ProfileNode profileNode) {
		this.profileNode = profileNode;
		ticketBookingService = new TicketBookingServiceImpl();
		view = new TicketBookingView(this, showTimeId);
	}

	@Override
	public ShowTime getShowTime(int showTimeId) {
		return ticketBookingService.getShowTime(showTimeId);
	}

	@Override
	public void savePayment(Payment payment) {
		ticketBookingService.savePayment(payment);
	}

	@Override
	public void updateTicketsView() {
		profileNode.updateTicketsView();
	}

}
