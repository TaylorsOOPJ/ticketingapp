package my.edu.taylors.oopj.movieticketing.entity;

public enum Accessibility {
	_0("0"),
	_13("13"),
	_15("15"),
	_18("18");

	private final String value;

	private Accessibility(String s) {
		value = s;
	}

	public boolean equalsValue(String otherValue) {
		return (otherValue == null) ? false : value.equals(otherValue);
	}

	public String toString() {
		return this.value;
	}

}
