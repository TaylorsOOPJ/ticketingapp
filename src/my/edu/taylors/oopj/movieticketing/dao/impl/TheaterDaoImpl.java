package my.edu.taylors.oopj.movieticketing.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import my.edu.taylors.oopj.movieticketing.dao.TheaterDao;
import my.edu.taylors.oopj.movieticketing.db.DatabaseConnection;
import my.edu.taylors.oopj.movieticketing.entity.Theater;

public class TheaterDaoImpl extends AbstractDao<Theater> implements TheaterDao {

	public static final String SQL = "t.id, t.theaterName, t.location";

	public static Theater getTheaterFromResult(ResultSet results) throws SQLException {
		int id = results.getInt("t.id");
		String theaterName = results.getString("t.theaterName");
		String location = results.getString("t.location");

		return new Theater(id, theaterName, location);
	}

	@Override
	public Theater getByKey(int key) {
		List<Theater> theaters = find("WHERE t.id = ?", key);
		return theaters.size() > 0 ? theaters.get(0) : null;
	}

	@Override
	public List<Theater> findAll() {
		return find("");
	}

	private List<Theater> find(String where, Object... args) {

		try {
			Connection conn = DatabaseConnection.getInstance().getConnection();
			String sql = 
				"SELECT " + SQL + " " +
				"FROM theater t " + where;
			PreparedStatement selectStatement = conn.prepareStatement(sql);
			for (int i = 0; i < args.length; i++) {
				selectStatement.setObject(i+1, args[i]);
			}

			ResultSet results = selectStatement.executeQuery();
			List<Theater> theaters = new ArrayList<>();

			while (results.next()) {
				Theater theater = getTheaterFromResult(results);
				theaters.add(theater);
			}

			return theaters;
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return new ArrayList<>(0);
	}

}
