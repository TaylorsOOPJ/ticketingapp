package my.edu.taylors.oopj.movieticketing.view;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.control.TextField;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import my.edu.taylors.oopj.movieticketing.controller.MovieViewController;
import my.edu.taylors.oopj.movieticketing.controller.TicketDetailController;
import my.edu.taylors.oopj.movieticketing.entity.Customer;
import my.edu.taylors.oopj.movieticketing.entity.Payment;
import my.edu.taylors.oopj.movieticketing.utils.AlertFactory;
import my.edu.taylors.oopj.movieticketing.view.dto.UpdateCustomerDto;
import my.edu.taylors.oopj.movieticketing.view.listener.ProfileListener;

public class ProfileNode extends MyNode {

	private ProfileListener profileListener;
	private Customer customer;

	private TextField tfFirstName, tfLastName, tfEmail, tfPhone;
	private Button btSaveCustomer;
	private PasswordField pfPassword, pfPasswordConfirm;
	private Button btSavePassword;
	private GridPane grid = new GridPane();

	public ProfileNode(ProfileListener profileListener) {
		this.profileListener = profileListener;
		customer = profileListener.getLoggedCustomer();
		setupGui();
		updateTicketsView();
	}

	private void setupGui() {
		GridPane mainPane = new GridPane();
		mainPane.getStyleClass().add("main-bg");

		ColumnConstraints column1 = new ColumnConstraints();
		column1.setPercentWidth(60);
		mainPane.getColumnConstraints().add(column1);

		ColumnConstraints column2 = new ColumnConstraints();
		column2.setPercentWidth(40);
		mainPane.getColumnConstraints().add(column2);

		mainPane.add(getTicketsBox(), 0, 0);
		mainPane.add(getUserInfoBox(), 1, 0);
		mainNode = mainPane;
	}

	private Node getUserInfoBox() {

		GridPane grid = new GridPane();
		grid.setVgap(10);
		grid.setPadding(new Insets(10));
		grid.getStyleClass().add("main-bg");

		ColumnConstraints column1 = new ColumnConstraints();
		column1.setPercentWidth(30);
		grid.getColumnConstraints().add(column1);

		ColumnConstraints column2 = new ColumnConstraints();
		column2.setPercentWidth(70);
		grid.getColumnConstraints().add(column2);
		

		Label lbFirstName = new Label("First name");
		lbFirstName.setId("h2");
		grid.add(lbFirstName, 0, 0);
		lbFirstName.setPadding(new Insets(0, 10, 0, 0));
		GridPane.setHalignment(lbFirstName, HPos.RIGHT);

		tfFirstName = new TextField(customer.getFirstName());
		grid.add(tfFirstName, 1, 0);
		tfFirstName.setId("tfield");

		Label lbLastName = new Label("Last name");
		lbLastName.setId("h2");
		grid.add(lbLastName, 0, 1);
		lbLastName.setPadding(new Insets(0, 10, 0, 0));
		GridPane.setHalignment(lbLastName, HPos.RIGHT);

		tfLastName = new TextField(customer.getLastName());
		grid.add(tfLastName, 1, 1);
		tfLastName.setId("tfield");

		Label lbEmail = new Label("Email");
		lbEmail.setId("h2");
		grid.add(lbEmail, 0, 2);
		lbEmail.setPadding(new Insets(0, 10, 0, 0));
		GridPane.setHalignment(lbEmail, HPos.RIGHT);

		tfEmail = new TextField(customer.getEmail());
		tfEmail.setId("tfield");
		grid.add(tfEmail, 1, 2);

		Label lbPhone = new Label("Phone");
		lbPhone.setId("h2");
		grid.add(lbPhone, 0, 3);
		lbPhone.setPadding(new Insets(0, 10, 0, 0));
		GridPane.setHalignment(lbPhone, HPos.RIGHT);

		tfPhone = new TextField(Integer.toString(customer.getPhone()));
		tfPhone.setId("tfield");
		grid.add(tfPhone, 1, 3);

		btSaveCustomer = new Button("Save");
		btSaveCustomer.setId("smallbutton");
		btSaveCustomer.setOnAction(e -> saveCustomer());
		grid.add(btSaveCustomer, 0, 4, 2, 1);
		GridPane.setHalignment(btSaveCustomer, HPos.RIGHT);

		Label lbPassword = new Label("New password");
		lbPassword.setId("h2");
		grid.add(lbPassword, 0, 5);
		lbPassword.setPadding(new Insets(0, 10, 0, 0));
		GridPane.setHalignment(lbPassword, HPos.RIGHT);

		pfPassword = new PasswordField();
		pfPassword.setId("tfield");
		grid.add(pfPassword, 1, 5);

		Label lbPasswordConfirm = new Label("Confirm password");
		lbPasswordConfirm.setId("h2");
		grid.add(lbPasswordConfirm, 0, 6);
		lbPasswordConfirm.setPadding(new Insets(0, 10, 0, 0));

		GridPane.setHalignment(lbPasswordConfirm, HPos.RIGHT);
		pfPasswordConfirm = new PasswordField();
		pfPasswordConfirm.setId("tfield");
		grid.add(pfPasswordConfirm, 1, 6);

		btSavePassword = new Button("Change password");
		btSavePassword.setId("smallbutton");
		btSavePassword.setOnAction(e -> changePassword());
		grid.add(btSavePassword, 0, 7, 2, 1);
		GridPane.setHalignment(btSavePassword, HPos.RIGHT);

		Label lbHeading = new Label("My Details");
		lbHeading.setPadding(new Insets(10));
		lbHeading.getStyleClass().add("heading");

		VBox vbox = new VBox(lbHeading, grid);
		vbox.setStyle("-fx-background-color: #37474F;-fx-border-style: none;");
		ScrollPane scroll = new ScrollPane(vbox);
		scroll.setStyle("-fx-background-color: #37474F;-fx-border-style: none;");
		scroll.setHbarPolicy(ScrollBarPolicy.NEVER);
		scroll.setFitToWidth(true);

		return scroll;
	}

	private void saveCustomer() {
		btSaveCustomer.setDisable(true);
		String firstName = tfFirstName.getText().trim();
		String lastName = tfLastName.getText().trim();
		String email = tfEmail.getText().trim();
		String phoneString = tfPhone.getText().trim();

		if (firstName.equals("") || lastName.equals("") || email.equals("") || phoneString.equals("")) {
			fieldError("Fill all fields before saving");
		} else {
			try {
				int phone = Integer.parseInt(phoneString);
				profileListener.updateCustomer(new UpdateCustomerDto(firstName, lastName, phone, email));
			} catch (NumberFormatException ex) {
				fieldError("Phone must be a number.");
			}
		}
	}

	private void changePassword() {
		btSavePassword.setDisable(true);
		String password = pfPassword.getText().trim();
		String passwordConfirm = pfPasswordConfirm.getText().trim();
		if (password.equals(passwordConfirm)) {
			profileListener.updatePassword(password);
		} else {
			fieldError("Passwords do not match.");
		}
	}

	private Node getTicketsBox() {

		grid = new GridPane();

		ColumnConstraints column1 = new ColumnConstraints();
		column1.setPercentWidth(40);
		grid.getColumnConstraints().add(column1);

		ColumnConstraints column2 = new ColumnConstraints();
		column2.setPercentWidth(20);
		grid.getColumnConstraints().add(column2);

		ColumnConstraints column3 = new ColumnConstraints();
		column3.setPercentWidth(20);
		grid.getColumnConstraints().add(column3);

		ColumnConstraints column4 = new ColumnConstraints();
		column4.setPercentWidth(20);
		grid.getColumnConstraints().add(column4);

		grid.setVgap(10);
		grid.setPadding(new Insets(10));
		grid.getStyleClass().add("main-bg");

		Label lbHeading = new Label("My Tickets");
		lbHeading.setPadding(new Insets(10));
		lbHeading.getStyleClass().add("heading");

		VBox vbox = new VBox(lbHeading, grid);
		vbox.getStyleClass().add("main-bg");
		ScrollPane scroll = new ScrollPane(vbox);
		scroll.getStyleClass().add("main-bg");
		scroll.setHbarPolicy(ScrollBarPolicy.NEVER);
		scroll.setFitToWidth(true);

		return scroll;
	}

	public void updateTicketsView() {
		grid.getChildren().clear();
		List<Payment> tickets = profileListener.getTicketsByCustomer(customer);
		
		if (tickets.size() == 0) {
			Label lbNotFound = new Label("You haven't purchased any tickets yet.");
			lbNotFound.setId("h3");
			StackPane paneNotFound = new StackPane(lbNotFound);
			paneNotFound.getStyleClass().add("grid-bg");
			grid.add(paneNotFound, 0, 1, 4, 1);
		}

		for (int i = 0; i < tickets.size(); i++) {
			final Payment p = tickets.get(i);

			Hyperlink hlMovieTitle = new Hyperlink(p.getShowtimeID().getMovieID().getMovieName());
			hlMovieTitle.setOnAction(e -> showMovieDetail(p.getShowtimeID().getMovieID().getId()));
			hlMovieTitle.setId("h3");
			StackPane paneTitle = new StackPane(hlMovieTitle);
			paneTitle.getStyleClass().add("grid-bg");
			grid.add(paneTitle, 0, i);

			String time = getTime(p.getShowtimeID().getTime());

			Label lbTime = new Label(time);
			lbTime.setId("h2");
			StackPane paneTime = new StackPane(lbTime);
			paneTime.setPrefWidth(20);
			paneTime.getStyleClass().add("grid-bg");
			grid.add(paneTime, 1, i);

			String countText = Integer.toString(p.getNumTicket()) + " ticket";
			if (p.getNumTicket() > 1) countText += "s";
			Label lbCount = new Label(countText);
			lbCount.setId("h2");
			StackPane paneCount = new StackPane(lbCount);
			paneCount.getStyleClass().add("grid-bg");
			grid.add(paneCount, 2, i);

			Button btBuy = new Button("Ticket detail");
			btBuy.setId("smallbutton");
			StackPane paneBuy = new StackPane(btBuy);
			paneBuy.getStyleClass().add("grid-bg");
			btBuy.setOnAction(e -> showPaymentDetail(p.getId()));
			grid.add(paneBuy, 3, i);
		}
	}

	private void showPaymentDetail(int id) {
		new TicketDetailController(id);
	}

	private void showMovieDetail(int id) {
		new MovieViewController(id);
	}

	private void fieldError(String error) {
		btSaveCustomer.setDisable(false);
		btSavePassword.setDisable(false);
		AlertFactory.createAlert(Alert.AlertType.ERROR, "Field error", error);
	}

	public void customerUpdateSuccessful() {
		btSaveCustomer.setDisable(false);
		btSavePassword.setDisable(false);
		AlertFactory.createAlert(Alert.AlertType.INFORMATION, "Changed", "Your information was updated.");
	}

	private String getTime(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int monthInt = cal.get(Calendar.MONTH)+1;
		String month = (monthInt < 10 ? "0" : "") + monthInt;
		int dayInt = cal.get(Calendar.DAY_OF_MONTH);
		String day = (dayInt < 10 ? "0" : "") + dayInt;
		int hour = cal.get(Calendar.HOUR);
		int minute = cal.get(Calendar.MINUTE);
		String AM_PM = cal.get(Calendar.AM_PM) == Calendar.AM ? "am" : "pm";
		if (hour == 0) hour = 12;
		String time = day + "/" + month + " " +hour + "." + minute + " " + AM_PM;

		return time;
	}

}
