package my.edu.taylors.oopj.movieticketing.controller;

import my.edu.taylors.oopj.movieticketing.view.View;

public abstract class AbstractController {

	protected View view;
	
	protected AbstractController() { }

	public View getView() {
		return view;
	}

	public void setView(View view) {
		this.view = view;
	}

}
