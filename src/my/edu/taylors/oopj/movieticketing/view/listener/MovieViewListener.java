package my.edu.taylors.oopj.movieticketing.view.listener;

import java.util.List;

import my.edu.taylors.oopj.movieticketing.entity.Genre;
import my.edu.taylors.oopj.movieticketing.entity.Movie;
import my.edu.taylors.oopj.movieticketing.entity.ShowTime;

public interface MovieViewListener {

	Movie getMovie(int id);

	List<Genre> getMovieGenres(int movieId);

	List<ShowTime> getShowTimesByMovieAndTheater(int movieId, int currentTheater);

}
