package my.edu.taylors.oopj.movieticketing.view;

import java.util.Calendar;
import java.util.Date;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import my.edu.taylors.oopj.movieticketing.entity.Payment;
import my.edu.taylors.oopj.movieticketing.entity.ShowTime;
import my.edu.taylors.oopj.movieticketing.utils.AlertFactory;
import my.edu.taylors.oopj.movieticketing.view.listener.TicketBookingListener;

/**
 * @author Johnny Chan
 * @author Milan Kostak
 */
public class TicketBookingView extends View {
	
    private Label lbTotal2;
    private TextField tfQty;
    private Button btBuy;
    private boolean canBuy = false;
    private final ShowTime showTime;
    private TicketBookingListener ticketBookingListener;

    public TicketBookingView(TicketBookingListener ticketBookingListener, int showTimeId) {
    	this.ticketBookingListener = ticketBookingListener;

        showTime = ticketBookingListener.getShowTime(showTimeId);

    	final String headingStyle = "-fx-text-fill: #FFFFFF; -fx-font-size: 25px; -fx-font-family: Raleway; -fx-font-weight: 300";
    	final String normalStyle  = "-fx-text-fill: #FFFFFF; -fx-font-size: 16px; -fx-font-family: Raleway";

        GridPane ticketBookingPane = new GridPane();

        //-------------Adding components to the gridpane----------------

        Label lbMovieName = new Label(showTime.getMovieID().getMovieName());
        lbMovieName.setStyle(headingStyle);
        ticketBookingPane.add(lbMovieName, 0, 0, 2, 1);

        // price
        Label lbPrice = new Label("Price per ticket ");
        lbPrice.setStyle(normalStyle);
        Label lbPrice2 = new Label("RM"+showTime.getPrice());
        lbPrice2.setStyle(normalStyle);
        ticketBookingPane.add(lbPrice, 0, 1);
        ticketBookingPane.add(lbPrice2, 1, 1);

        // date & time
        Label lbDate = new Label("Date and time ");
        lbDate.setStyle(normalStyle);
        Label lbDate2 = new Label(getDate(showTime.getTime()));
        lbDate2.setStyle(normalStyle);
        ticketBookingPane.add(lbDate, 0, 2);
        ticketBookingPane.add(lbDate2, 1, 2);

        // quantity
        Label lbQty = new Label("Number of tickets ");
        lbQty.setStyle(normalStyle);
        tfQty = new TextField();
        tfQty.setOnKeyReleased(e -> countTickets());
        ticketBookingPane.add(lbQty, 0, 3);
        ticketBookingPane.add(tfQty, 1, 3);

        // total
        Label lbTotal = new Label("Total");
        lbTotal.setStyle(headingStyle);
        lbTotal2 = new Label("RM0");
        lbTotal2.setStyle(normalStyle);
        ticketBookingPane.add(lbTotal, 0, 4);
        ticketBookingPane.add(lbTotal2, 1, 4);

        // button
        btBuy = new Button("BUY");
        btBuy.setStyle("-fx-border-radius: 6; -fx-color: #263238; -fx-text-fill: #FFFFFF; "
                + "-fx-width: 20px; -fx-font-family: Raleway; -fx-font-size: 20px; -fx-font-weight: 300");
        ticketBookingPane.add(btBuy, 1, 6);
        GridPane.setHalignment(btBuy, HPos.RIGHT);
        btBuy.setOnAction(e -> buy());

        // Styling Pane
        ticketBookingPane.setStyle("-fx-background-color: #37474F");
        ticketBookingPane.setPadding(new Insets(15));
        ticketBookingPane.setHgap(15);
        ticketBookingPane.setVgap(15);

        // Set scene
        Scene ticketBookingScene = new Scene(ticketBookingPane);
        ticketBookingScene.getStylesheets().add("https://fonts.googleapis.com/css?family=Raleway:300,400,700");
        setScene(ticketBookingScene);
        setTitle("Buy tickets for " + showTime.getMovieID().getMovieName());

        setModality();
    }

    private void buy() {
		if (!canBuy) {
			AlertFactory.createAlert(Alert.AlertType.ERROR, "Error", "Fill properly the amount of tickets to buy.");
		} else {
			String count = tfQty.getText();
			int cnt = Integer.parseInt(count);
			if (cnt == 0) {
				AlertFactory.createAlert(Alert.AlertType.ERROR, "Error", "Amount cannot be 0.");
			} else {
				btBuy.setDisable(true);
				float total = showTime.getPrice() * cnt;
				Payment payment = new Payment(new Date(), cnt, total, HomeView.customer, showTime);
				ticketBookingListener.savePayment(payment);
				AlertFactory.createAlert(Alert.AlertType.INFORMATION, "Success", "You just bought "+cnt+" tickets.");
				ticketBookingListener.updateTicketsView();
				hide();
			}
		}
	}

	private void countTickets() {
		String count = tfQty.getText();
		if (!count.equals("")) {
			try {
				int cnt = Integer.parseInt(count);
				double total = showTime.getPrice() * cnt;
				String ttl = String.format("%.2f", total);
				lbTotal2.setText("RM"+ttl);
				canBuy = true;
			} catch (NumberFormatException e) {
				lbTotal2.setText("-");
				canBuy = false;
			}
		} else {
			lbTotal2.setText("-");
			canBuy = false;
		}
	}

	private String getDate(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);

		int year = cal.get(Calendar.YEAR);

		int monthInt = cal.get(Calendar.MONTH)+1;
		String month = (monthInt < 10 ? "0" : "") + monthInt;

		int dayInt = cal.get(Calendar.DAY_OF_MONTH);
		String day = (dayInt < 10 ? "0" : "") + dayInt;

		int hour = cal.get(Calendar.HOUR);
		int minute = cal.get(Calendar.MINUTE);
		String AM_PM = cal.get(Calendar.AM_PM) == Calendar.AM ? "am" : "pm";
		if (hour == 0) hour = 12;
		String time = day + "/" + month + "/" + year + " " +hour + "." + minute + " " + AM_PM;

		return time;
    }

}
