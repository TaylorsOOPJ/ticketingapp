
package my.edu.taylors.oopj.movieticketing.service.impl;

import java.util.List;
import my.edu.taylors.oopj.movieticketing.dao.MovieDao;
import my.edu.taylors.oopj.movieticketing.dao.impl.MovieDaoImpl;
import my.edu.taylors.oopj.movieticketing.entity.Movie;
import my.edu.taylors.oopj.movieticketing.service.HotsService;

public class HotsServiceImpl implements HotsService{
    private MovieDao movieDao;

    public HotsServiceImpl() {
        movieDao = new MovieDaoImpl();
    }

    @Override
    public List<Movie> getHotMovies() {
        return movieDao.findAllByHot();
    }

}
