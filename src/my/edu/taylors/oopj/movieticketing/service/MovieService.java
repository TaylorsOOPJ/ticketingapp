package my.edu.taylors.oopj.movieticketing.service;

import java.util.List;

import my.edu.taylors.oopj.movieticketing.entity.Genre;
import my.edu.taylors.oopj.movieticketing.entity.Movie;
import my.edu.taylors.oopj.movieticketing.entity.ShowTime;

public interface MovieService {

	Movie getMovie(int id);

	List<Genre> getMovieGenres(int movieId);

	List<ShowTime> getShowTimesByMovieAndTheater(int movieId, int currentTheater);

}
