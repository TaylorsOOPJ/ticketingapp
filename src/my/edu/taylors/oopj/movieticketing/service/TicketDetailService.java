package my.edu.taylors.oopj.movieticketing.service;

import my.edu.taylors.oopj.movieticketing.entity.Payment;

public interface TicketDetailService {

	Payment getPaymentById(int paymentId);

}
