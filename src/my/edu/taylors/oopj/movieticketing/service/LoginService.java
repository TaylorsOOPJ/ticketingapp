package my.edu.taylors.oopj.movieticketing.service;

import my.edu.taylors.oopj.movieticketing.entity.Customer;

public interface LoginService {

	boolean isCustomerEmailDuplicate(Customer customer);

	boolean authenticate(String email, String password);

	void insertCustomer(Customer entity);

	Customer getCustomerByEmail(String email);

}
