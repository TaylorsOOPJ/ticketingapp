package my.edu.taylors.oopj.movieticketing.controller;

import java.util.List;

import javafx.scene.Node;
import my.edu.taylors.oopj.movieticketing.entity.Genre;
import my.edu.taylors.oopj.movieticketing.entity.Movie;
import my.edu.taylors.oopj.movieticketing.service.GenresService;
import my.edu.taylors.oopj.movieticketing.service.impl.GenresServiceImpl;
import my.edu.taylors.oopj.movieticketing.view.GenresNode;
import my.edu.taylors.oopj.movieticketing.view.listener.GenresListener;

public class GenresController implements GenresListener {

	private GenresService genresService;
	private GenresNode genresNode;

	public GenresController() {
		genresService = new GenresServiceImpl();
		genresNode = new GenresNode(this);
	}

	@Override
	public Node getMainNode() {
		return genresNode.getMainNode();
	}

	@Override
	public List<Movie> getAllMovies() {
		return genresService.getAllMovies();
	}

	@Override
	public List<Genre> getAllGenres() {
		return genresService.getAllGenres();
	}

	@Override
	public List<Movie> getMoviesByGenre(int selectedGenre) {
		return genresService.getMoviesByGenre(selectedGenre);
	}
}
