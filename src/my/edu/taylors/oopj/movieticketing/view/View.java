package my.edu.taylors.oopj.movieticketing.view;

import javafx.stage.Modality;
import javafx.stage.Stage;

public class View extends Stage {

	protected View() { }

	protected void setModality() {
		setResizable(false);
		initModality(Modality.WINDOW_MODAL);
		initOwner(HomeView.homeStage);
		show();
	}

}
