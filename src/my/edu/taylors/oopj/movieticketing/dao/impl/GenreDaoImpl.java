package my.edu.taylors.oopj.movieticketing.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import my.edu.taylors.oopj.movieticketing.dao.GenreDao;
import my.edu.taylors.oopj.movieticketing.db.DatabaseConnection;
import my.edu.taylors.oopj.movieticketing.entity.Genre;

public class GenreDaoImpl extends AbstractDao<Genre> implements GenreDao {

	public static final String SQL = "g.id, g.name";

	public static Genre getGenreFromResult(ResultSet results) throws SQLException {
		int id = results.getInt("g.id");
		String name = results.getString("g.name");

		return new Genre(id, name);
	}

	@Override
	public Genre getByKey(int key) {
		List<Genre> genres = find("WHERE g.id = ?", key);
		return genres.size() > 0 ? genres.get(0) : null;
	}

	@Override
	public List<Genre> findAll() {
		return find("");
	}

	private List<Genre> find(String where, Object... args) {

		try {
			Connection conn = DatabaseConnection.getInstance().getConnection();
			String sql = 
				"SELECT " + SQL + " " +
				"FROM genre g " + where;
			PreparedStatement selectStatement = conn.prepareStatement(sql);
			for (int i = 0; i < args.length; i++) {
				selectStatement.setObject(i+1, args[i]);
			}

			ResultSet results = selectStatement.executeQuery();
			List<Genre> genres = new ArrayList<>();

			while (results.next()) {
				Genre genre = getGenreFromResult(results);
				genres.add(genre);
			}

			return genres;
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return new ArrayList<>(0);
	}

	@Override
	public List<Genre> getMovieGenres(int movieId) {
		try {
			Connection conn = DatabaseConnection.getInstance().getConnection();
			String sql = 
				"SELECT " + SQL + " " +
				"FROM genre g " +
				"INNER JOIN movie_genre mg ON mg.genreID = g.id " +
				"WHERE mg.movieID = ?";
			PreparedStatement selectStatement = conn.prepareStatement(sql);
			selectStatement.setObject(1, movieId);

			ResultSet results = selectStatement.executeQuery();
			List<Genre> genres = new ArrayList<>();

			while (results.next()) {
				Genre genre = getGenreFromResult(results);
				genres.add(genre);
			}

			return genres;
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return new ArrayList<>(0);
	}

}
