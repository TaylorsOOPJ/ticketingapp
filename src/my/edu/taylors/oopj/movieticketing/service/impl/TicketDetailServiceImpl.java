package my.edu.taylors.oopj.movieticketing.service.impl;

import my.edu.taylors.oopj.movieticketing.dao.PaymentDao;
import my.edu.taylors.oopj.movieticketing.dao.impl.PaymentDaoImpl;
import my.edu.taylors.oopj.movieticketing.entity.Payment;
import my.edu.taylors.oopj.movieticketing.service.TicketDetailService;

public class TicketDetailServiceImpl implements TicketDetailService {

	private PaymentDao paymentDao;

	public TicketDetailServiceImpl() {
		paymentDao = new PaymentDaoImpl();
	}

	@Override
	public Payment getPaymentById(int paymentId) {
		return paymentDao.getByKey(paymentId);
	}

}
