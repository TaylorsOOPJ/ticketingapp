package my.edu.taylors.oopj.movieticketing;

import java.util.*;

import my.edu.taylors.oopj.movieticketing.dao.*;
import my.edu.taylors.oopj.movieticketing.dao.impl.*;
import my.edu.taylors.oopj.movieticketing.db.DatabaseConnection;
import my.edu.taylors.oopj.movieticketing.entity.*;

public class DBtest {

	public static void main(String[] args) throws Exception {

		DatabaseConnection.getInstance().connect();
		Calendar cal = Calendar.getInstance();

		CustomerDao customerDao = new CustomerDaoImpl();
		TheaterDao theaterDao = new TheaterDaoImpl();
		MovieDao movieDao = new MovieDaoImpl();
		ShowTimeDao showTimeDao = new ShowTimeDaoImpl();
		PaymentDao paymentDao = new PaymentDaoImpl();
		GenreDao genreDao = new GenreDaoImpl();

		///////////////

		Customer a = customerDao.getByKey(1);
		Customer b = customerDao.getByKey(1);
		
		Set<Customer> set = new HashSet<>();
		set.add(a);
		set.add(b);
		System.out.println(set);
		
		List<Customer> list = new ArrayList<>();
		list.add(a);
		list.add(b);
		System.out.println(list);

		System.out.println(customerDao.findAll());

		Customer cust = new Customer("Jack", "Bauer", "asdfasdf@vmtdfbgdbg.com", "hash", 13254681, new Date());
		customerDao.insert(cust);
		System.out.println(cust);
		
		cust.setFirstName(Integer.toString((int)(Math.random()*1000)));
		customerDao.update(cust);
		System.out.println(cust);
		
		customerDao.delete(cust);

		System.out.println(customerDao.findAll());

		///////////////

		System.out.println(theaterDao.findAll());
		
		Theater th = new Theater("nevim nevim", "Thailand");
		theaterDao.insert(th);
		System.out.println(th);
		
		th.setLocation("asdf");
		theaterDao.update(th);
		System.out.println(th);
		
		theaterDao.delete(th);

		System.out.println(theaterDao.findAll());

		///////////////

		System.out.println(movieDao.findAll());

		Calendar cal2 = Calendar.getInstance();
		cal2.set(Calendar.YEAR, 2016);
		cal2.set(Calendar.MONTH, 10);
		cal2.set(Calendar.DAY_OF_MONTH, 10);
		
		String name = "It's Only the End of the World";
		String desc = "Louis (Gaspard Ulliel), a terminally ill writer, returns home after a long absence to tell his family that he is dying.";
		String img = "https://images-na.ssl-images-amazon.com/images/M/MV5BMzk5OWJkZDItODdmMS00ZWIyLWJmYjUtOTQyYzlmNDk4ZDY0XkEyXkFqcGdeQXVyNTAzMDcxMjM@._V1_SY1000_CR0,0,692,1000_AL_.jpg";
		String imdb = "http://www.imdb.com/title/tt4645368/";
		String trailer = "https://youtu.be/mLDP3U7gpnw/";
		Movie mv = new Movie(name, 7, desc, Accessibility._13, cal2.getTime(), imdb, img, false, trailer);
		movieDao.insert(mv);
		System.out.println(mv);
		
		mv = movieDao.getByKey(mv.getId());
		mv.setDescription("asdf");
		movieDao.update(mv);
		System.out.println(mv);

		movieDao.delete(mv);

		System.out.println(movieDao.findAll());

		///////////////

		System.out.println(showTimeDao.findAll());

		Theater theater = theaterDao.getByKey(1);
		Movie movie = movieDao.getByKey(1);
		
		
		ShowTime st = new ShowTime(cal.getTime(), 25.9f, 120, movie, theater);
		showTimeDao.insert(st);

		System.out.println(st);
		
		st.setPrice(9.9f);
		showTimeDao.update(st);
		System.out.println(st);

		showTimeDao.delete(st);

		System.out.println(showTimeDao.findAll());
		
		///////////////

		System.out.println(paymentDao.findAll());

		ShowTime st2 = showTimeDao.getByKey(7);
		Customer c2 = customerDao.getByKey(90);
		Payment p = new Payment(cal.getTime(), 3, 3*9.9f, c2, st2);

		paymentDao.insert(p);
		System.out.println(p);
		
		p.setNumTicket(4);
		paymentDao.update(p);
		System.out.println(p);

		paymentDao.delete(p);

		System.out.println(paymentDao.findAll());

		///////////////

		System.out.println(genreDao.getMovieGenres(1));

		///////////////

		System.out.println("DONE. IF NO ERROR THEN PASSED.");

	}
}
